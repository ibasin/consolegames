﻿using System;
using GameEngine;

namespace Jump
{
    public class Player : TangibleGameObject
    {
        #region Constructors
        public Player()
        {
            #region Body Sprite
            Body = new Sprite();
            //Head
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Yellow));

            //Shoulders and arms
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));

            //Torso
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));

            //Legs
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 3), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 3), ' ', ConsoleColor.Black, ConsoleColor.DarkMagenta));
            #endregion

            #region Initial location
            Location = new Point(0, Console.WindowHeight - Body.Size.Y - 1);
            #endregion
        }
        #endregion
        
        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.ReadKey();

            Point newLocation = Location;
            if (key != null)
            {
                if (key.Value.Key == ConsoleKey.RightArrow) newLocation = Location.Move(DirectionEnum.Right);
                if (key.Value.Key == ConsoleKey.LeftArrow) newLocation = Location.Move(DirectionEnum.Left);
            }

            if (Body.IsFullyValidAtLocation(newLocation)) Location = newLocation;
        }

        public override void Draw()
        {
            Body.Draw(Location);
        }

        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite Body { get; protected set; }
        #endregion
    }
}
