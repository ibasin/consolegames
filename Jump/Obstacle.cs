﻿using System;
using GameEngine;

namespace Jump
{
    public class Obstacle : TangibleGameObject
    {
        #region Constructors
        public Obstacle(int x, ConsoleColor fgColor, ConsoleColor bgColor)
        {
            FgColor = fgColor;
            BgColor = bgColor;
            X = x;

            Height = Game.Rnd.Next(2, 5);
        }
        #endregion
        
        #region Overrdies
        public override void Update()
        {
           //do nothing
        }
        public override void Draw()
        {
            Body.Draw(new Point(X, Console.WindowHeight - Body.Size.Y));
            //if (_isFirstDraw)
            //{
            //    Body.Draw(new Point(X, Console.WindowHeight - Body.Size.Y));
            //    _isFirstDraw = false;
            //}
        }
        public override void Erase()
        {
            //do nothing
        }
        #endregion

        #region Properties
        public int X { get; }
        public ConsoleColor FgColor { get; set; }
        public ConsoleColor BgColor { get; set; }

        public Sprite Body 
        { 
            get
            {
                if (_body == null)
                {
                    _body = new Sprite();
                    for(var y = TopPoint.Y; y < Console.WindowHeight; y++)
                    {
                        _body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, y - TopPoint.Y), ' ', FgColor, BgColor));
                    }
                }
                return _body;
            }
        }
        protected Sprite _body;

        public int Height { get; set; }

        public Point TopPoint
        {
            get
            {
                if (_topPoint == null)
                {
                    _topPoint = new Point(X, Console.WindowHeight - 1 - Height);
                }
                return _topPoint.Value;
            }
        }
        protected Point? _topPoint;

        protected bool _isFirstDraw = true;
        #endregion
    }
}
