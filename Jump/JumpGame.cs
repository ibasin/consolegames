﻿using GameEngine;
using System;
using System.Collections.Generic;

namespace Jump
{
    public class JumpGame : Game
    {
        #region Constructors
        public JumpGame()
        {
            Current = this;
            
            //generate player
            GameObjects.Add(new Player());

            //generate the buildings
            for(var x = 5; x <= Console.WindowWidth - 5; x += 10)
            {
                var obstacle = new Obstacle(x, ConsoleColor.Green, ConsoleColor.Green);
                Obstacles.Add(obstacle);
                GameObjects.Add(obstacle);
            }
        }
        #endregion

        #region Properties
        public static JumpGame Current { get; protected set; }
        public List<Obstacle> Obstacles { get; } = new List<Obstacle>();
        #endregion
    }
}
