﻿using System;
using System.IO;
using GameEngine;

namespace MazeEscaper
{
	public class MazeEscaperMaze : TangibleGameObject
	{
        #region Constructors/Factory Methods
        public MazeEscaperMaze(string filepath)
		{
			Body = new Sprite();

			if (!File.Exists(filepath))
			{
				throw new Exception("Maze file does not exist!");
			}
			else
			{
				using (StreamReader sr = File.OpenText(filepath))
				{
					string s;
					int x = 0, y = 0; // coords for map

					while ((s = sr.ReadLine()) != null)
					{
						foreach (var character in s)
						{
							if (char.IsWhiteSpace(character))
							{
								x++;
								continue;
							}
							else
							{
								ConsoleColor color = ConsoleColor.White;
								if (character >= 0x2500 && character <= 0x257F)
								{
									color = ConsoleColor.Cyan;
								}

								Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), (char)character, color));
							}

							x++;
						}

						x = 0;
						y++;
					}

				}
			}
		}
		#endregion

		#region Overrides
		public override void Erase()
		{
			var key = Game.KeyboardManager.ReadKey();

			Point futureLocation = Location;
			if (key.HasValue)
			{
				var k = key.Value.Key;
				if (k == ConsoleKey.UpArrow) futureLocation = Location.Move(DirectionEnum.Down);
				if (k == ConsoleKey.DownArrow) futureLocation = Location.Move(DirectionEnum.Up);
				if (k == ConsoleKey.LeftArrow) futureLocation = Location.Move(DirectionEnum.Right);
				if (k == ConsoleKey.RightArrow) futureLocation = Location.Move(DirectionEnum.Left);
			}
			
			if (Body.IsCollidingAtLocation(futureLocation, MazeEscaperGame.Current.MazeEscaperPlayer.Body, MazeEscaperGame.Current.MazeEscaperPlayer.Location))
			{
				futureLocation = Location;
			}
			ShouldRedraw = futureLocation != Location;
			if (ShouldRedraw) Body.Erase(Location, ConsoleColor.Black);
			Location = futureLocation;

			// If map gets changed, change this point to match win condition
			if (MazeEscaperGame.Current.Map.Location == new Point(-20, -15))
			{
				throw new GameOverException("You Won, Congrats!");
			}
			if (MazeEscaperGame.Current.Map.Location == new Point(-20, -16))
			{
				throw new GameOverException("You Won, Congrats!");
			}
		}
		public override void Update()
		{
			//Clean out the buffer
			while (Game.KeyboardManager.ReadKey().HasValue) { /* do nothing */}
		}
		public override void Draw()
		{
			if (ShouldRedraw || FirstIteration)
			{
				Body.Draw(Location);
				FirstIteration = false;
			}
		}
		#endregion

		#region Methods
		public void Clear()
		{
			Body.PointsWithCharAndColor.Clear();
		}
		#endregion

		#region Properties
		protected Sprite Body { get; }
		protected Point Location { get; set; } = new Point(0, 0);

		protected bool FirstIteration { get; set; } = true;
		protected bool ShouldRedraw { get; set; }
		#endregion
	}
}
