﻿using System;
using GameEngine;

namespace MazeEscaper
{
    class MazeEscaperPlayer : TangibleGameObject
    {
        #region Constructors
        public MazeEscaperPlayer(Point location)
        {
            Location = location;
            Body = new Sprite();

            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Red));
        }
        #endregion

        #region Methods/Overrides
        public override void Update() { }

        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; set; }
        #endregion
    }
}
