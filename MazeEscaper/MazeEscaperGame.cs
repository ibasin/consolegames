﻿using GameEngine;

namespace MazeEscaper
{
    class MazeEscaperGame : Game
    {
        #region Constructors
        public MazeEscaperGame()
        {
            MazeEscaperPlayer = new MazeEscaperPlayer(new Point(2, 2));
            GameObjects.Add(MazeEscaperPlayer);


            Map = new MazeEscaperMaze("MazeEscaperMaze.txt");
            GameObjects.Add(Map);
            Current = this;
        }
        #endregion

        #region Properties
        public static MazeEscaperGame Current { get; protected set; }

        public MazeEscaperPlayer MazeEscaperPlayer { get; set; }
        public MazeEscaperMaze Map { get; set; }
        #endregion
    }
}
