﻿using GameEngine;
using System;
namespace EndlessRunner
{
    public class Obstacle : TangibleGameObject
    {
        #region Constructors
        public Obstacle (Point location)
        {
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero.Move(DirectionEnum.Down, 1), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero.Move(DirectionEnum.Down, 2), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero.Move(DirectionEnum.Down, 3), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Left);
            if (Location.Violations.IsInViolation) ToDelete = true;

            if (Body.IsCollidingAtLocation(Location, EndlessRunnerGame.Current.Player.Body, EndlessRunnerGame.Current.Player.Location))
            {
                throw new GameOverException($"You hit a cactus, Game Over. Score = {EndlessRunnerGame.Current.ScoreKeeper.Score}");
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; }
        #endregion
    }
}
