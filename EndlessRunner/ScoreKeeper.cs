﻿using GameEngine;
using System;

namespace EndlessRunner
{
    public class ScoreKeeper : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Score += 1;
            Point.Zero.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write($"Score: {Score}");
        }
        #endregion

        #region Properties
        public int Score { get; set; }
        #endregion
    }
}
