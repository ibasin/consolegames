﻿using GameEngine;
using System;

namespace EndlessRunner
{
    public class ObstacleSpawner : GameObject
    {
        #region Overrides
        public override void Update()
        {
            if (_coolDownCounter == 0)
            {
                var chance = EndlessRunnerGame.Rnd.Next(9);
                if (chance == 0)
                {
                    var obstacle = new Obstacle(new Point(Console.WindowWidth - 1, Console.WindowHeight - 4 - 4));
                    EndlessRunnerGame.Current.GameObjects.Add(obstacle);
                    _coolDownCounter = 12;
                }
            }
            else
            {
                _coolDownCounter--;
                if (_coolDownCounter < 0) _coolDownCounter = 0;
            }

        }
        #endregion

        #region Properties
        protected int _coolDownCounter = 0;
        #endregion
    }
}
