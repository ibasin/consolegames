﻿using GameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndlessRunner
{
    public class Player : TangibleGameObject
    {
        #region Constructors
        public Player(Point location)
        {
            Body = new Sprite();

            //Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.White, ConsoleColor.White));
            //Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, Console.WindowHeight), ' ', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), '(', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 4), 'L', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 3), 'T', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 3), 'I', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), 'I', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 2), ']', ConsoleColor.White));


            Location = new Point (10, Console.WindowHeight - 4 - 5);
            FloorAltitude = Altitude = Location.Y;
        }
        #endregion
        #region Overrides
        public override void Update()
        {
            var key = EndlessRunnerGame.KeyboardManager.ReadKey();

            if (key.HasValue && key.Value.Key == ConsoleKey.Spacebar && Vvert == 0 && Altitude >= FloorAltitude) Vvert = -1.8;

            Altitude += Vvert;
            if (Altitude >= FloorAltitude)
            {
                Altitude = FloorAltitude;
                Vvert = 0;
            }
            else
            {
                Vvert += G;
            }

            Location = new Point(Location.X, (int)Math.Round(Altitude));
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion
        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; }
        public double Altitude { get; set; }
        public double FloorAltitude { get; }
        public double Vvert { get; set; }
        public const double G = 0.25; //Acceleration of falling
        #endregion
    }
}
