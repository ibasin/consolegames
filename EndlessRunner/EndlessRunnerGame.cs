﻿using System;
using GameEngine;

namespace EndlessRunner
{
    class EndlessRunnerGame : Game
    {
        #region Constructors
        public EndlessRunnerGame()
        {
            GameObjects.Add(new ObstacleSpawner());
            
            Player = new Player(new Point());
            GameObjects.Add(Player);
            Current = this;

            ScoreKeeper = new ScoreKeeper();
            GameObjects.Add(ScoreKeeper);

            Primitives.DrawHorizontalLine(Console.WindowHeight - 4, 0, Console.WindowWidth-1, ' ', ConsoleColor.Green, ConsoleColor.Green);

        }
        #endregion
        #region Properties
        public static EndlessRunnerGame Current { get; private set; }
        public Player Player { get; }
        public ScoreKeeper ScoreKeeper { get; }
        //public Obstacle Obstacle { get; }
        #endregion
    }
}
