﻿using System;
using System.IO;
using GameEngine;

namespace Pac_Man
{
    public class Map : TangibleGameObject
    {
        #region Constructors
        public Map()
        {
            Body = new Sprite();

            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '_', ConsoleColor.White));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '|', ConsoleColor.White));
        }

        public static Map Load(string filepath)
        {
            Map rtn = new Map();

            if (!File.Exists(filepath)) throw new Exception("File does not exist");

            using (StreamReader sr = File.OpenText(filepath))
            {
                string s;
                int x = 0, y = 0;

                while ((s = sr.ReadLine()) != null)
                {
                    foreach( int character in s)
                    {
                        if(character == ' ')
                        {
                            x++;
                            continue;
                        }
                        else
                        {
                            ConsoleColor color = ConsoleColor.White;
                            if (character >= 0x2500 && character <= 0x257F)
                            {
                                color = ConsoleColor.White;
                            }

                            rtn.Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), (char)character, color));
                        }
                        x++;
                    }
                    x = 0;
                    y++;
                }

            }
            return rtn;

        }
        #endregion

        #region Override
        public override void Update()
        {
            //do nothing
        }

        public override void Draw()
        {
            if (New)
            {
                Body.Draw(Location);
                New = false;
            }
        }

        public override void Erase()
        {
            //do nothing
        }
        #endregion

        #region Properties
        protected bool New { get; set; } = true;
        public Sprite Body { get; }
        public Point Location { get; } = Point.Zero;
        #endregion
    }
}
