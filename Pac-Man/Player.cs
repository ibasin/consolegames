﻿using System;
using System.IO;
using System.Linq;
using GameEngine;

namespace Pac_Man
{
    public class Player : TangibleGameObject
    {
        #region Constructors
        public Player(Point location)
        {
            Location = location;
        }
        #endregion

        #region Overrides

        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("C");
        }

        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.Write(" ");
        }

        public override void Update()
        {
            var key = Game.KeyboardManager.ReadKey();

            if (key != null)
            {
                Point futureLocation; 
                var k = key.Value.Key;
                if (k == ConsoleKey.UpArrow) futureLocation = Location.Move(DirectionEnum.Up);
                else if (k == ConsoleKey.DownArrow) futureLocation = Location.Move(DirectionEnum.Down);
                else if (k == ConsoleKey.LeftArrow) futureLocation = Location.Move(DirectionEnum.Left);
                else if (k == ConsoleKey.RightArrow) futureLocation = Location.Move(DirectionEnum.Right);
                else return;

                if (futureLocation.Violations.IsInViolation) futureLocation = futureLocation.CorrectViolations();
                if (PacManGame.Current.Map.Body.IsCollidingAtLocation(PacManGame.Current.Map.Location, futureLocation)) futureLocation = Location;

                Location = futureLocation;

                var collidingFood = PacManGame.Current.Foods.SingleOrDefault(x => x.Location == Location);
                if (collidingFood != null)
                {
                    collidingFood.ToDelete = true;
                    PacManGame.Current.Foods.Remove(collidingFood);
                    if (PacManGame.Current.Foods.Count == 0) throw new GameOverException("You win!");
                }
            }
        }
        #endregion

        #region Methods
        #endregion

        #region Properties
        public Point Location { get; set; } = new Point(0, 0);

        protected bool FirstIteration { get; set; } = true;
        protected bool ShouldRedraw { get; set; }
        #endregion
    }
}
