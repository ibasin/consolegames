﻿using System;
using System.Linq;
using GameEngine;

namespace Pac_Man
{
    public class PacManFood : TangibleGameObject
    {
        #region Constructors
        public PacManFood(Point location)
        {
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            //do nothing
        }

        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("~");
        }

        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" ");
        }
        #endregion

        #region Properties
        public Point Location { get; set; } = new Point(0, 0);

        #endregion
    }
}
