﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEngine;

namespace Pac_Man
{
    public class PacManGame : Game
    {
        #region Constructors
        public PacManGame()
        {
            IterationSleep = 0;
            Map = Map.Load("pacManMap.txt");
            GameObjects.Add(Map);

            Player = new Player(new Point(40, 6));
            GameObjects.Add(Player);

            for (var i = 0; i < 5; i++)
            {
                var foodLocation = new Point(Rnd.Next(Map.Body.Size.X), Rnd.Next(Map.Body.Size.Y));
                if (Map.Body.IsCollidingAtLocation(Map.Location, foodLocation) ||
                    Foods.Any(x => x.Location == foodLocation) ||
                    foodLocation == Player.Location)
                {
                    i--;
                    continue;
                }
                var food = new PacManFood(foodLocation);
                Foods.Add(food);
                GameObjects.Add(food);
            }

            Ghost = new Ghost(new Point(5, 5));
            GameObjects.Add(Ghost);

            //This is just for testing. Keep commented out
            //var fixedFood = new PacManFood(new Point(4, 5));
            //Foods.Add(fixedFood);
            //GameObjects.Add(fixedFood);

            Current = this;
        }
        #endregion

        #region Properties
        public static PacManGame Current { get; protected set; }

        public List<PacManFood> Foods { get; } = new List<PacManFood>();
        public Player Player { get; }
        public Map Map { get; }
        public Ghost Ghost { get; }
        #endregion
    }
}
