﻿using System;
using System.Threading;
using GameEngine;

namespace Pac_Man
{
    public class Ghost : TangibleGameObject
    {
        #region Constructors
        public Ghost(Point location)
        {
            Location = location;
        }
        #endregion

        #region Updates
        public override void Update()
        {
            var random = Game.Rnd.Next(4);

            Point futureLocation;
            if (random == 0) futureLocation = Location.Move(DirectionEnum.Left);
            else if (random == 1) futureLocation = Location.Move(DirectionEnum.Right);
            else if (random == 2) futureLocation = Location.Move(DirectionEnum.Down);
            else if (random == 3) futureLocation = Location.Move(DirectionEnum.Up);
            else return;

            if (futureLocation.Violations.IsInViolation) futureLocation = futureLocation.CorrectViolations();
            if (PacManGame.Current.Map.Body.IsCollidingAtLocation(PacManGame.Current.Map.Location, futureLocation)) futureLocation = Location;

            Location = futureLocation;
        }

        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("A");
        }

        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" ");
        }
        #endregion

        #region Properties
        protected Point Location { get; set; } = new Point(0, 0);
        #endregion
    }
}
