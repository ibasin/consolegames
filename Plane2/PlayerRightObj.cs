﻿using GameEngine;
using System;

namespace PlaneGameTwo
{

    public class PlayerRightObj : TangibleGameObject
    {
        #region Constructors
        public PlayerRightObj(Point location)
        {
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '@', ConsoleColor.Magenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '.', ConsoleColor.Yellow));
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.PeekKey();
            if (!key.HasValue) return;

            var location = new Point(Location);
            if (key.Value.Key == ConsoleKey.Spacebar && CoolDownCounter == 0)
            {
                var bullet = new PlayerRightBulletObj(Location.Move(DirectionEnum.Down));
                PlaneGameTwoPlayer.Current.GameObjects.Add(bullet);
                CoolDownCounter = 3;
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.RightArrow) 
            {
                Location = Location.Move(DirectionEnum.Right);
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.LeftArrow) 
            {
                Location = Location.Move(DirectionEnum.Left);
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.UpArrow) 
            {
                Location = Location.Move(DirectionEnum.Up);
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.DownArrow) 
            {
                Location = Location.Move(DirectionEnum.Down);
                Game.KeyboardManager.ReadKey();
            }

            if (!Body.IsFullyValidAtLocation(Location)) Location = location;
            CoolDownCounter--;
            if (CoolDownCounter < 0) CoolDownCounter = 0;
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get;  set; }
        public Sprite Body { get; set; }
        protected int CoolDownCounter { get; set; }
        #endregion
    }
}

