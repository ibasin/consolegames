﻿using GameEngine;
using System;

namespace PlaneGameTwo
{

    public class PlayerLeftObj : TangibleGameObject
    {
        #region Constructors
        public PlayerLeftObj(Point location)
        {
            #region Sprite
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '@', ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), '.', ConsoleColor.Yellow));
            #endregion

            #region Location
            Location = location;
            #endregion
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.PeekKey();
            if (!key.HasValue) return;

            var location = new Point(Location);
            if (key.Value.Key == ConsoleKey.X && CoolDownCounter == 0)
            {
                var bullet = new PlayerLeftBulletObj (Location.Move(DirectionEnum.Right, 5).Move(DirectionEnum.Down));
                PlaneGameTwoPlayer.Current.GameObjects.Add(bullet);
                CoolDownCounter = 3;
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.D) 
            {
                Location = Location.Move(DirectionEnum.Right);
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.A) 
            {
                Location = Location.Move(DirectionEnum.Left);
                Game.KeyboardManager.ReadKey();
            }

            if (key.Value.Key == ConsoleKey.W) 
            {
                Location = Location.Move(DirectionEnum.Up);
                Game.KeyboardManager.ReadKey();
            }
            
            if (key.Value.Key == ConsoleKey.S) 
            {
                Location = Location.Move(DirectionEnum.Down);
                Game.KeyboardManager.ReadKey();
            }

            if (!Body.IsFullyValidAtLocation(Location)) Location = location;
            CoolDownCounter--;
            if (CoolDownCounter < 0) CoolDownCounter = 0;
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite Body { get; }
        protected int CoolDownCounter { get; set; }
        #endregion
    }
}

