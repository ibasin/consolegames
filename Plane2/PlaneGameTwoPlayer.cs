﻿using GameEngine;
using System;

namespace PlaneGameTwo
{
    public class PlaneGameTwoPlayer : Game
    {
        #region Constructors
        public PlaneGameTwoPlayer()
        {
            IterationSleep = 200;

            var y = Console.WindowHeight / 2 - 3 / 2;
            
            PlaneLeft = new PlayerLeftObj(new Point(0, y));
            GameObjects.Add(PlaneLeft);

            PlaneRight = new PlayerRightObj(new Point(Console.WindowWidth - 6, y));
            GameObjects.Add(PlaneRight);

            Current = this;
        }
        #endregion

        #region Properties
        public static PlaneGameTwoPlayer Current { get; private set; }
        public PlayerLeftObj PlaneLeft { get; }
        public PlayerRightObj PlaneRight { get; }
        #endregion
    }

}
