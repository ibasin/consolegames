﻿using System;
using GameEngine;

namespace PlaneGameTwo
{
    public class PlayerLeftBulletObj : TangibleGameObject
    {
        #region Constructors
        public PlayerLeftBulletObj(Point location)
        {
            Location = location;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, '-', ConsoleColor.Cyan));
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Right, 3);
            if (!Body.IsFullyValidAtLocation(Location)) ToDelete = true;

            var plane = PlaneGameTwoPlayer.Current.PlaneRight;
            if (Body.IsCollidingAtLocation(Location, plane.Body, plane.Location)) throw new GameOverException("Cyan wins!");
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }
        #endregion
    }
}
