﻿using System;
using GameEngine;

namespace PlaneGameTwo
{
    public class PlayerRightBulletObj : TangibleGameObject
    {
        #region Constructors
        public PlayerRightBulletObj(Point location)
        {
            Location = location;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, '-', ConsoleColor.Magenta));
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Left, 3);

            if (Body.IsFullyValidAtLocation(location)) Location = location;
            else ToDelete = true;

            var plane = PlaneGameTwoPlayer.Current.PlaneLeft;
            if (Body.IsCollidingAtLocation(Location, plane.Body, plane.Location)) throw new GameOverException("Magenta wins!");
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }

        #endregion
    }
}

