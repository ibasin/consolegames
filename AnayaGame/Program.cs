﻿using System;

namespace DungeonCrawl
{
	class Program
	{
		static void Main()
		{
			Console.WindowHeight = 21;
			Console.WindowWidth = 41;

			new DungeonCrawlGame().Run();
		}
	}
}
