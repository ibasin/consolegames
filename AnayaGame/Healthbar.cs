﻿using System;
using GameEngine;

namespace DungeonCrawl
{
	class HealthBar : TangibleGameObject
	{
        #region Overrides
        public override void Erase()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorTop = 0;
            Console.CursorLeft = 0;
            Console.Write("            ");
        }
        public override void Update()
        {
            //do nothing
        }
        public override void Draw()
		{
			Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorTop = 0;
            Console.CursorLeft = 0;
            Console.Write("Health: 100%");
		}
		#endregion

		#region Properties
		//protected Sprite Body { get; set; } = new Sprite();
		#endregion
	}
}
