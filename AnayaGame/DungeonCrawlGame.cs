﻿using GameEngine;

namespace DungeonCrawl
{
	class DungeonCrawlGame : Game
	{
		#region Constructors
        public DungeonCrawlGame()
		{
            IterationSleep = 25;
			GameObjects.Add(Map.Load("map.txt"));
			
            Player = new Player(new Point(21, 11));
			GameObjects.Add(Player);
			
            GameObjects.Add(new HealthBar());
            Current = this; 
		}
		#endregion

		#region Properties
		public static DungeonCrawlGame Current { get; protected set; }
		public Player Player { get; }
		#endregion
	}
}
