﻿using System;
using System.IO;
using GameEngine;

namespace DungeonCrawl
{
	public class Map : TangibleGameObject
	{
		#region Constructors/Factory Methods
        public Map()
		{
			Body = new Sprite();

			for (int i = 0; i < Console.WindowWidth; i++)
			{
				Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(i, 0), '═', ConsoleColor.Cyan));
			}

			for (int i = 0; i < Console.WindowHeight; i++)
			{
				Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, i), '║', ConsoleColor.Cyan));
			}

			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '╔', ConsoleColor.Cyan));
		}

		public static Map Load(string filepath)
		{
			Map rtn = new Map();
			rtn.Clear();

			if (!File.Exists(filepath))
			{
				return rtn;
			}
			else
			{
				using (StreamReader sr = File.OpenText(filepath))
				{
					string s;
					int x = 0, y = 0; // coords for map
					
					while ((s = sr.ReadLine()) != null)
					{
						foreach (int character in s)
						{
							if (character == ' ')
							{
								x++;
								continue;
							}
							else
							{
								ConsoleColor color = ConsoleColor.White;
								if (character >= 0x2500 && character <= 0x257F)
								{
									color = ConsoleColor.Cyan;
								}

								rtn.Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), (char) character, color));
							}

							x++;
						}

						x = 0;
						y++;
					}

				}

				return rtn;
			}
		}
		#endregion

		#region Overrides
		public override void Erase()
		{
            var key = Game.KeyboardManager.ReadKey();

            if (key.HasValue)
            {
                var k = key.Value.Key;
                if (k == ConsoleKey.UpArrow) FutureLocation = Location.Move(DirectionEnum.Down);
                if (k == ConsoleKey.DownArrow) FutureLocation = Location.Move(DirectionEnum.Up);
                if (k == ConsoleKey.LeftArrow) FutureLocation = Location.Move(DirectionEnum.Right);
                if (k == ConsoleKey.RightArrow) FutureLocation = Location.Move(DirectionEnum.Left);	
            }

            if (Body.IsCollidingAtLocation(FutureLocation, DungeonCrawlGame.Current.Player.Location)) FutureLocation = Location;
			ShouldRedraw = FutureLocation != Location;

            if (ShouldRedraw) Body.Erase(Location, ConsoleColor.Black);
        }
		public override void Update()
		{
			Location = FutureLocation;

			//Clean out the buffer
            while(Game.KeyboardManager.ReadKey().HasValue) { /* do nothing */}
		}
		public override void Draw()
		{
			if (ShouldRedraw || FirstIteration)
			{
				Body.Draw(Location);
				FirstIteration = false;
			}
		}
		#endregion

		#region Methods
		public void Clear()
		{
			Body.PointsWithCharAndColor.Clear();
		}
		#endregion

		#region Properties
		protected Sprite Body { get; }
		protected Point Location { get; set; } = new Point(0, 0);
		protected Point FutureLocation { get; set; } = new Point(0, 0);

		protected bool FirstIteration { get; set; } = true;
		protected bool ShouldRedraw { get; set; }
		#endregion
	}
}
