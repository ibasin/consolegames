﻿using System;
using GameEngine;

namespace DungeonCrawl
{
	public class Player : TangibleGameObject
	{
		#region Constructors
        public Player(Point location)
		{
			Body = new Sprite();
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '@', ConsoleColor.Yellow));

			Location = location;
		}
		#endregion

		#region Overrides
        public override void Erase()
		{
            //do nothing
		}
		public override void Update()
		{
			//do nothing
		}
		public override void Draw()
		{
			Body.Draw(Location);
		}
		#endregion

		#region Properties
        public Point Location { get; set; }
		protected Sprite Body { get; }
		#endregion
	}
}
