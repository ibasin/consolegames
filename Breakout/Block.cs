﻿using System;
using GameEngine;

namespace Breakout
{
    public class Block : TangibleGameObject
    {
        #region Constructors
        public Block(Point location, int blockLevel)
        {
            //Sprite
            Body = new Sprite();
            BlockLevel = blockLevel;
            for (int y = 0; y < 3; y++)
            {
                for (var x = 0; x < 12; x++)
                {
                    Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), ' ', ConsoleColor.Black, GetColorByLevel(BlockLevel)));
                }
            }

            //Location
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            if (Body.IsCollidingAtLocation(Location, BreakoutGame.Current.Ball.Body, BreakoutGame.Current.Ball.Location))
            {
                Console.Beep();

                if (IsVerticalHit()) BreakoutGame.Current.Ball.VY = -BreakoutGame.Current.Ball.VY;
                else BreakoutGame.Current.Ball.VX = -BreakoutGame.Current.Ball.VX;

                BlockLevel--;
                if (BlockLevel <= 0)
                {
                    ToDelete = true;
                }
                else
                {
                    Newborn = 2;
                    var newSprite = new Sprite();
                    foreach (var pointWithCharAndColor in Body.PointsWithCharAndColor)
                    {
                        var newPoint = new PointWithCharAndColor(
                            pointWithCharAndColor.Point,
                            pointWithCharAndColor.Chr,
                            pointWithCharAndColor.FgColor,
                            GetColorByLevel(BlockLevel));

                        newSprite.PointsWithCharAndColor.Add(newPoint);

                        Body = newSprite;
                    }
                }

                var foundOne = false;
                foreach (var gameObject in BreakoutGame.Current.GameObjects)
                {
                    if (gameObject is Block && !gameObject.ToDelete)
                    {
                        foundOne = true;
                        break;
                    }
                }

                if (!foundOne) throw new GameOverException("You won!");
            }
        }
        protected bool IsVerticalHit()
        {
            //find slope and offset
            var m = BreakoutGame.Current.Ball.VY / BreakoutGame.Current.Ball.VX; //slope
            var b = BreakoutGame.Current.Ball.Y - m*BreakoutGame.Current.Ball.X; //offset

            //figure out if we use top or bottom line for the block and find Y for it
            double blockY;
            if (BreakoutGame.Current.Ball.VY > 0) blockY = Location.Y;
            else blockY = Location.Y + Body.Size.Y + 1;

            //find intersection x coordinate
            var x = (blockY - b) / m;

            //see if we cross the line within the block's x range
            return x >= Location.X && x <= Location.X + Body.Size.X + 1;
        }

        public override void Draw()
        {
            if (Newborn > 0)
            {
                Body.Draw(Location);
                Newborn--;
            }
        }

        public override void Erase()
        {
            if (ToDelete) Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Methods
        private static ConsoleColor GetColorByLevel(int blockLevel)
        {
            switch (blockLevel)
            {
                case 1: return ConsoleColor.Green;
                case 2: return ConsoleColor.DarkYellow;
                case 3: return ConsoleColor.DarkRed;
                default: throw new Exception("We only handle 1-3 blockLevels");
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; protected set; }
        public int BlockLevel { get; private set; }

        public int Newborn { get; protected set; } = 1;
        #endregion
    }
}
