﻿using System;
using GameEngine;

namespace Breakout
{
    public class Ball : TangibleGameObject
    {
        #region Constructors
        public Ball(Point location)
        {
            //Sprite
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Red));

            //Location
            Location = location;
            X = Location.X;
            Y = Location.Y;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = BreakoutGame.KeyboardManager.PeekKey();
            if (!key.HasValue) return;
            
            if (key.Value.Key == ConsoleKey.Spacebar && IsAtRest())
            {
                VX = 0.5;
                VY = -1;
            }

            X += VX;
            Y += VY;
            
            Location = new Point((int)Math.Round(X), (int)Math.Round(Y));
            
            if (Location.Violations.XPlusViolation || Location.Violations.XMinusViolation)
            {
                Location = Location.CorrectViolations();
                VX = -VX;
            }

            if (Location.Violations.YMinusViolation)
            {
                Location = Location.CorrectViolations();
                VY = -VY;
            }

            if (Location.Violations.YPlusViolation) throw new GameOverException("You lost! Sad!");
        }

        public override void PostUpdate()
        {
            Erase();
            if (IsAtRest())
            {
                Location = BreakoutGame.Current.Paddle.Location.Move(DirectionEnum.Right, Paddle.HalfWidth).Move(DirectionEnum.Up);
                X = Location.X;
                Y = Location.Y;
            }
            Draw();
        }

        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Methods
        public bool IsAtRest()
        {
            return VX == 0 && VY == 0;
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; }

        public double X { get; set; }
        public double Y { get; set; }

        // ReSharper disable InconsistentNaming
        public double VX { get; set; }
        public double VY { get; set; }
        // ReSharper restore InconsistentNaming
        #endregion
    }
}
