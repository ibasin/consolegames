﻿using System;
using GameEngine;

namespace Breakout
{
    public class Paddle : TangibleGameObject
    {
        #region Constructors
        public Paddle(Point location)
        {
            //Sprite
            Body = new Sprite();

            //Do not forget to update HalfWidth property when changing the size of the paddle
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(6, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(7, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(8, 0), ' ', ConsoleColor.Black, ConsoleColor.Cyan));

            //Location
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = BreakoutGame.KeyboardManager.PeekKey();
            if (!key.HasValue) return;

            var oldLocation = Location;

            const int paddleSpeed = 5;
            if (key.Value.Key == ConsoleKey.LeftArrow) Location = Location.Move(DirectionEnum.Left, paddleSpeed);
            if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right, paddleSpeed);

            var isColliding = false;
            if (Body.IsCollidingAtLocation(Location, BreakoutGame.Current.Ball.Body, BreakoutGame.Current.Ball.Location))
            {
                BreakoutGame.Current.Ball.VY = -BreakoutGame.Current.Ball.VY;
                isColliding = true;
            }

            if (key.Value.Key == ConsoleKey.LeftArrow)
            {
                BreakoutGame.KeyboardManager.ReadKey();
                if (!BreakoutGame.Current.Ball.IsAtRest() && isColliding) BreakoutGame.Current.Ball.VX -= 0.7;
            }

            if (key.Value.Key == ConsoleKey.RightArrow)
            {
                BreakoutGame.KeyboardManager.ReadKey();
                if (!BreakoutGame.Current.Ball.IsAtRest() && isColliding) BreakoutGame.Current.Ball.VX += 0.7;
            }

            if (!Body.IsFullyValidAtLocation(Location))
            {
                if (Location.X < 0)
                {
                    Location = Location.CorrectViolations();
                }
                else
                {

                    Location = Location
                        .Move(DirectionEnum.Right, Body.Size.X-1)
                        .CorrectViolations()
                        .Move(DirectionEnum.Left, Body.Size.X-1);
                }

            }
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; }
        public static int HalfWidth => 4;
        #endregion
    }
}