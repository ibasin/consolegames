﻿using System;
using GameEngine;

namespace Breakout
{
    public class BreakoutGame : Game
    {
        #region Constructors
        public BreakoutGame()
        {
            Paddle = new Paddle(new Point(Console.WindowWidth / 2 - Paddle.HalfWidth, Console.WindowHeight - 2));
            GameObjects.Add(Paddle);

            Ball = new Ball(new Point(Console.WindowWidth / 2, Console.WindowHeight - 3));
            GameObjects.Add(Ball);

            for (var y = 4; y <= 12; y += 4)
            {
                for (var x = 1; x+12 < Console.WindowWidth; x += 13)
                {
                    var block = new Block(new Point(x, y), 3);
                    GameObjects.Add(block);
                }
            }

            Current = this;
        }
        #endregion

        #region Properties
        public static BreakoutGame Current { get; private set; }

        public Paddle Paddle { get;  }
        public Ball Ball { get; }
        #endregion
    }
}
