﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    public class Bar : TangibleGameObject
    {
        #region Constructors
        public Bar(Point location)
        {
            //Sprite
            Body = new Sprite();
            for (int y = 0; y < 5; y++)
            {
                for (var x = 0; x < Console.WindowWidth; x++)
                {
                    Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), ' ', ConsoleColor.Black, ConsoleColor.DarkYellow));
                }
            }

            //Location
            Location = location;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            
        }
        
        public override void Draw()
        {
            Body.Draw(Location);
        }

        public override void Erase()
        {
            if (ToDelete) Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; }
        public static int BarHalf => 2;
        #endregion
    }
}
