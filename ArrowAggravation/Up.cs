﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    class Up : Arrow
    {
        #region Constructors
        public Up(Point location)
        {
            //Sprite
            Body = new Sprite();

            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 3), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 4), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '^', ConsoleColor.Cyan, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 2), '^', ConsoleColor.Cyan, ConsoleColor.Black));

            Location = location;

            MyKey = ConsoleKey.UpArrow;
        }
        #endregion
    }
}
