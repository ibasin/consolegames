﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    class Left : Arrow
    {
        #region Constructors
        public Left(Point location)
        {
            //Sprite
            Body = new Sprite();
            
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 2), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 2), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 3), '<', ConsoleColor.Magenta, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 4), '<', ConsoleColor.Magenta, ConsoleColor.Black));

            Location = location;

            MyKey = ConsoleKey.LeftArrow;
        }
        #endregion
    }
}
