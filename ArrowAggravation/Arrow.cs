﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    abstract class Arrow : TangibleGameObject
    {
        #region Overrides
        public override void Update()
        {
            const int arrowSpeed = 2;
            Location = Location.Move(DirectionEnum.Down, arrowSpeed);
            
            if (!Body.IsFullyValidAtLocation(Location))
            {
                if (Location.Y < 0)
                {
                    Location = Location.CorrectViolations();
                }
                else
                {
                    ToDelete = true;
                    ArrowGame.Miss++;
                    if (ArrowGame.Miss >= 10)
                    {
                        throw new GameOverException($"You were doing so well! (But you still lost somehow) You hit {ArrowGame.ArrowsHit} arrow(s).");
                    }
                }
            }

            var key = ArrowGame.KeyboardManager.PeekKey();

            if (!key.HasValue) return;

            if (Body.IsCollidingAtLocation(Location, ArrowGame.Current.Bar.Body, ArrowGame.Current.Bar.Location))
            {
                if (key.Value.Key == MyKey)
                {
                    ToDelete = true;
                    ArrowGame.ArrowsHit++;
                    Console.Beep();
                }
                else
                {
                    ArrowGame.Current.Bar.Update();
                }

                ArrowGame.KeyboardManager.ReadKey();
            }
        }
        
        public override void Draw()
        {
            if (!ToDelete)
            {
                Body.Draw(Location);
            }
        }

        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite Body { get; set; }
        public ConsoleKey MyKey { get; set; }

        public static int ArrowHalf => 2;
        #endregion
    }
}
