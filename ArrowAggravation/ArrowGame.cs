﻿using System;
using System.Collections.Generic;
using GameEngine;

namespace ArrowAggravation
{
    public class ArrowGame : Game
    {
        #region Constructors
        public ArrowGame()
        {
            GameObjects.Add(new ArrowGenerator());
            GameObjects.Add(new Score());
            GameObjects.Add(new Loss());
            Bar = new Bar(new Point(0, Console.WindowHeight / 2 - Bar.BarHalf));
            GameObjects.Add(Bar);
            Current = this;
        }
        #endregion

        #region Properties
        public static ArrowGame Current { get; private set; }
        public static int ArrowsHit { get; set; }
        public static int Miss { get; set; }
        public Bar Bar { get; }
        #endregion
    }
}
