﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    public class ArrowGenerator : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Random rnd = new Random();
            int direction = rnd.Next(1, 21);
            if (direction == 1)
            {
                ArrowGame.Current.GameObjects.Add(new Right(new Point((Console.WindowWidth / 8) - Arrow.ArrowHalf , 0)));
            }
            if (direction == 2)
            {
                ArrowGame.Current.GameObjects.Add(new Left(new Point(3 * (Console.WindowWidth / 8) - Arrow.ArrowHalf, 0)));
            }
            if (direction == 3)
            {
                ArrowGame.Current.GameObjects.Add(new Up(new Point(5 * (Console.WindowWidth / 8) - Arrow.ArrowHalf, 0)));
            }
            if (direction == 4)
            {
                ArrowGame.Current.GameObjects.Add(new Down(new Point(7 * (Console.WindowWidth / 8) - Arrow.ArrowHalf, 0)));
            }
        }
        #endregion
    }
}
