﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    public class Score : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(0, 0);
            Console.Write($"Score: {ArrowGame.ArrowsHit}");
        }
        #endregion
    }
}