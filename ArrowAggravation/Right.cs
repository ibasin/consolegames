﻿using System;
using GameEngine;
namespace ArrowAggravation
{
    class Right : Arrow
    {
        #region Constructors
        public Right(Point location)
        {
            //Sprite
            Body = new Sprite();

            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 2), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 2), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 3), '>', ConsoleColor.Green, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 4), '>', ConsoleColor.Green, ConsoleColor.Black));

            Location = location;

            MyKey = ConsoleKey.RightArrow;
        }
        #endregion
    }
}
