﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    class Down : Arrow
    {
        #region Constructors
        public Down(Point location)
        {
            //Sprite
            Body = new Sprite();

            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 3), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 4), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 3), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 3), 'v', ConsoleColor.Red, ConsoleColor.Black));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 2), 'v', ConsoleColor.Red, ConsoleColor.Black));

            Location = location;

            MyKey = ConsoleKey.DownArrow;
        }
        #endregion
    }
}
