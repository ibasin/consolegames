﻿using System;
using GameEngine;

namespace ArrowAggravation
{
    public class Loss : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(Console.WindowWidth - 25, 0);
            Console.Write($"Misses: {ArrowGame.Miss} (Don't get 10!)");
        }
        #endregion
    }
}
