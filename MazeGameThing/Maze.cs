﻿using System;
using System.IO;
using GameEngine;

namespace MazeGameThing
{
	class Maze : TangibleGameObject
	{
		#region Constructors
		public Maze()
		{
			Body = new Sprite();
		}

		public static Maze Load(string filepath)
		{
			Maze rtn = new Maze();

			if (!File.Exists(filepath))
			{
				return rtn;
			}
			else
			{
				using (StreamReader sr = File.OpenText(filepath))
				{
					string s;
					int x = 0, y = 0; // coords for map

					while ((s = sr.ReadLine()) != null)
					{
						foreach (int character in s)
						{
							if (character == ' ')
							{
								x++;
								continue;
							}
							else
							{
								ConsoleColor color = ConsoleColor.White;
								if (character >= 0x2500 && character <= 0x257F)
								{
									color = ConsoleColor.Cyan;
								}

								rtn.Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), (char)character, color));
							}

							x++;
						}

						x = 0;
						y++;
					}

				}

				return rtn;
			}
		}
		#endregion

		#region Overrides
		public override void Erase()
		{
		
		}
		public override void Draw()
		{
			Body.Draw(Location);
		}
		public override void Update()
		{

		}
		#endregion

		#region Properties
		public Sprite Body { get; }
		public Point Location { get; set; } = new Point(0, 0);
		#endregion
	}
}
