﻿using System;
using GameEngine;

namespace MazeGameThing
{
	class Player : TangibleGameObject
	{
		#region Constructors
		public Player(Point location)
		{
			//Sprite
			Body = new Sprite();
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Green));

			//Location
			Location = location;
		}
		#endregion

		#region Overrides
		public override void Update()
		{
			var location = new Point(Location);

			//Controls
			var key = MazeGameThing.KeyboardManager.ReadKey();

			if (key != null)
			{
				if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right);
				if (key.Value.Key == ConsoleKey.LeftArrow) Location = Location.Move(DirectionEnum.Left);
				if (key.Value.Key == ConsoleKey.DownArrow) Location = Location.Move(DirectionEnum.Down);
				if (key.Value.Key == ConsoleKey.UpArrow) Location = Location.Move(DirectionEnum.Up);

				//Check if touching wall
				if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (key.Value.Key == ConsoleKey.RightArrow)) Location = Location.Move(DirectionEnum.Left);
				if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (key.Value.Key == ConsoleKey.LeftArrow)) Location = Location.Move(DirectionEnum.Right);
				if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (key.Value.Key == ConsoleKey.DownArrow)) Location = Location.Move(DirectionEnum.Up);
				if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (key.Value.Key == ConsoleKey.UpArrow)) Location = Location.Move(DirectionEnum.Down);
			}
		}

		public override void PostUpdate()
		{
			base.PostUpdate();
			var key = MazeGameThing.KeyboardManager.ReadKey();
			
		}	

		public override void Draw()
		{
			Body.Draw(Location);
		}
		public override void Erase()
		{
			Body.Erase(Location, ConsoleColor.Black);
		}
		#endregion

		#region Methods
		#endregion

		#region Properties
		public Point Location { get; set; }
		public Sprite Body { get; }
		#endregion
	}
}
