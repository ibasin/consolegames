﻿using System;
using GameEngine;

namespace MazeGameThing
{
	class MazeGameThing : Game
	{
		#region Constructors
		public MazeGameThing()
		{
			Current = this;
			Player = new Player(new Point(new Point(1,28)));
			GameObjects.Add(Player);
			Maze = Maze.Load("Maze.txt");
			GameObjects.Add(Maze);
			GameObjects.Add(new Finish(new Point(29,1)));

			for (int i = 0; i<16; i++)
			{
				var rndWindowHeight = Game.Rnd.Next(28);
				var rndWindowWidth = Game.Rnd.Next(65);

				Enemy = new Enemy(new Point(rndWindowWidth+1, rndWindowHeight+1));
				GameObjects.Add(Enemy);
			}
		}
		#endregion

		#region Properties
		public static MazeGameThing Current { get; private set; }
		public Enemy Enemy { get; set; }
		public Player Player { get; set; }
		public Maze Maze { get; set; }
		#endregion
	}
}
