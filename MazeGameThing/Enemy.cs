﻿using System;
using GameEngine;

namespace MazeGameThing
{
	class Enemy : TangibleGameObject
	{
		#region Constructors
		public Enemy(Point location)
		{
			//Sprite
			Body = new Sprite();
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Red));
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Red));
			//Location
			Location = location;
		}
		#endregion

		#region Overrides
		public override void Update()
		{
			//Check if player collided
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Player.Body, MazeGameThing.Current.Player.Location)) throw new GameOverException($"You ran into an enemy. Game over.");


			//Random Movement
			var rnd = Game.Rnd.Next(5);

			if (rnd == 0) Location = Location.Move(DirectionEnum.Right);
			if (rnd == 1) Location = Location.Move(DirectionEnum.Left);
			if (rnd == 2) Location = Location.Move(DirectionEnum.Down);
			if (rnd == 3) Location = Location.Move(DirectionEnum.Up);

			//Check if touching wall
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (rnd == 0)) Location = Location.Move(DirectionEnum.Left);
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (rnd == 1 )) Location = Location.Move(DirectionEnum.Right);
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (rnd == 2)) Location = Location.Move(DirectionEnum.Up);
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location) & (rnd == 3)) Location = Location.Move(DirectionEnum.Down);
			else if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Maze.Body, MazeGameThing.Current.Maze.Location)) Location = Location.Move(DirectionEnum.Right);
		}

		public override void Draw()
		{
			Body.Draw(Location);
		}
		public override void Erase()
		{
			Body.Erase(Location, ConsoleColor.Black);
		}
		#endregion

		#region Methods
		#endregion

		#region Properties
		public Point Location { get; set; }
		public Sprite Body { get; }
		#endregion
	}
}
