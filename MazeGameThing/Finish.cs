﻿using System;
using GameEngine;

namespace MazeGameThing
{
	class Finish : TangibleGameObject
	{
		#region Constructors
		public Finish(Point location)
		{
			//Sprite
			Body = new Sprite();
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', ConsoleColor.Black, ConsoleColor.Blue));
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.Black, ConsoleColor.Blue));
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Blue));
			Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.Black, ConsoleColor.Blue));

			//Location
			Location = location;
		}
		#endregion

		#region Overrides
		public override void Update()
		{
			//Check if player won
			if (Body.IsCollidingAtLocation(Location, MazeGameThing.Current.Player.Body, MazeGameThing.Current.Player.Location)) throw new GameOverException($"You won!");
		}
		public override void Draw()
		{
			Body.Draw(Location);
		}
		public override void Erase()
		{
			Body.Erase(Location, ConsoleColor.Black);
		}
		#endregion

		#region Properties
		public Point Location { get; set; }
		public Sprite Body { get; }
		#endregion
	}
}
