﻿using System;
using System.Collections.Generic;
using GameEngine;

namespace Bomber
{
    public class BomberGame : Game
    {
        #region Constructors
        public BomberGame()
        {
            Current = this;
            
            //generate plane
            GameObjects.Add(new Plane());
            
            //generate the buildings
            for(var x = 20; x < Console.WindowWidth - 20; x += 3)
            {
                var building = new Building(x, ConsoleColor.Green, ConsoleColor.Green);
                Buildings.Add(building);
                GameObjects.Add(building);
            }
        }
        #endregion

        #region Properties
        public static BomberGame Current { get; protected set; }
        public List<Building> Buildings { get; } = new List<Building>();
        #endregion
    }
}
