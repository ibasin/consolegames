﻿using System;
using System.Linq;
using GameEngine;

namespace Bomber
{
    public class Plane : TangibleGameObject
    {
        #region Constructors
        public Plane()
        {
            Location = Point.Zero;
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '@', ConsoleColor.Magenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), '.', ConsoleColor.Yellow));
        }
        #endregion
        
        #region Overrides
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Right);
            if (!Body.IsFullyValidAtLocation(Location)) Location = new Point(0, Location.Y + 1);
            if (BomberGame.Current.Buildings.Any(x => Body.IsCollidingAtLocation(Location, x.TopPoint))) throw new GameOverException("You lost! Your plane hit a building.");
            var key = Game.KeyboardManager.ReadKey();
            if (key != null && key.Value.Key == ConsoleKey.Spacebar && BomberGame.Current.GameObjects.All(x => !(x is Bomb)))
            {
                var bomb = new Bomb(Location.Move(DirectionEnum.Right, 3).Move(DirectionEnum.Down, 2), 1, 0.5, ConsoleColor.White, ConsoleColor.Black);
                BomberGame.Current.GameObjects.Add(bomb);
            }
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite Body { get; protected set; }
        #endregion
    }
}
