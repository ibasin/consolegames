﻿using System;
using GameEngine;

namespace Bomber
{
    public class Building : TangibleGameObject
    {
        #region Constructors
        public Building(int x, ConsoleColor fgColor, ConsoleColor bgColor)
        {
            FgColor = fgColor;
            BgColor = bgColor;
            X = x;
            Height = BomberGame.Rnd.Next(2, Console.WindowHeight / 2);
        }
        #endregion
        
        #region Overrdies
        public override void Update()
        {
           //do nothing
        }
        public override void Draw()
        {
            if (_buildingUpdated) ForceDraw();
            _buildingUpdated = false;
        }
        public override void Erase()
        {
            if (_buildingUpdated) ForceErase();
        }
        public void ForceDraw()
        {
            Body.Draw(TopPoint);
        }
        public void ForceErase()
        {
            Body.Erase(TopPoint, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public int X { get; }
        public ConsoleColor FgColor { get; set; }
        public ConsoleColor BgColor { get; set; }

        public Sprite Body 
        { 
            get
            {
                if (_body == null)
                {
                    _body = new Sprite();
                    for(var y = TopPoint.Y; y < Console.WindowHeight; y++)
                    {
                        _body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, y - TopPoint.Y), ' ', FgColor, BgColor));
                    }
                }
                return _body;
            }
        }
        public Sprite _body;

        public int Height 
        { 
            get => _height;
            set
            {
                _buildingUpdated = true;
                _topPoint = null;
                _body = null;
                _height = value;
            }
        }
        protected int _height;
        protected bool _buildingUpdated;

        public Point TopPoint
        {
            get
            {
                if (_topPoint == null)
                {
                    _topPoint = new Point(X, Console.WindowHeight - 1 - Height);
                }
                return _topPoint.Value;
            }
        }
        protected Point? _topPoint;
        #endregion
    }
}
