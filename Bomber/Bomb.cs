﻿using System;
using System.Linq;
using GameEngine;

namespace Bomber
{
    public class Bomb : TangibleGameObject
    {
        #region Constructors
        public Bomb(Point location, int vx, double vy, ConsoleColor fgColor, ConsoleColor bgColor)
        {
            Location = location;
            Vx = vx;
            Vy = vy;
            FgColor = fgColor;
            BgColor = bgColor;
        }
        #endregion
        
        #region Overrdies
        public override void Update()
        {
            Vy += G;
            Location = Location.Move(DirectionEnum.Right, Vx).Move(DirectionEnum.Down, (int)Math.Round(Vy));
            if (Location.Violations.IsInViolation) ToDelete = true;

            //check if the bomb hit a building
            var buildingHit = BomberGame.Current.Buildings.SingleOrDefault(x => x.Body.IsCollidingAtLocation(x.TopPoint, Location));
            if (buildingHit != null)
            {
                buildingHit.ForceErase();
                buildingHit.Height = Console.WindowHeight - 1 - Location.Y - 3;
                if (buildingHit.Height <= 0) 
                {
                    buildingHit.ToDelete = true;
                    BomberGame.Current.Buildings.Remove(buildingHit);
                    if (BomberGame.Current.Buildings.Count == 0) throw new GameOverException("You won! All buildings are destroyed.");
                }
                else
                {
                    buildingHit.ForceDraw();
                }
                ToDelete = true;
            }
        }
        public override void Draw()
        {
            if (!ToDelete && !Location.Violations.IsInViolation)
            {
                Location.SetCursorPosition();
                Console.ForegroundColor = FgColor;
                Console.BackgroundColor = BgColor;
                Console.Write('*');
            }
        }
        public override void Erase()
        {
            if (!Location.Violations.IsInViolation)
            {
                Location.SetCursorPosition();
                Console.ForegroundColor = FgColor;
                Console.BackgroundColor = BgColor;
                Console.Write(' ');
            }
        }
        #endregion

        #region Properties & Constants
        const double G = 0.25;

        public int Vx { get; protected set; }
        public double Vy { get; protected set; }
        public Point Location { get; set; }
        public ConsoleColor FgColor { get; }
        public ConsoleColor BgColor { get; }
        #endregion
    }
}
