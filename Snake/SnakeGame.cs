﻿using GameEngine;

namespace Snake
{
    public class SnakeGame : Game
    {
        #region Constructors
        public SnakeGame()
        {
            Current = this;
            Snake = new SnakeObj();
            Rabbit = RabbitObj.CreateAtRandomLocation();

            GameObjects.Add(new ScoreKeeper());
            GameObjects.Add(Snake);
            IterationSleep = 150;
        }
        #endregion

        #region Properties
        public static SnakeGame Current { get; protected set; }
        public static int Score { get; set; }
        public SnakeObj Snake { get; set; }
        public RabbitObj Rabbit { get; set; }
        #endregion
    }
}
