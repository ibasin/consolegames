﻿using System;
using GameEngine;

namespace Snake
{
    public class RabbitObj
    {
        #region Constructors
        public RabbitObj(Point location)
        {
            Location = location;

            Location.SetCursorPosition();
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Black;
        }
        public static RabbitObj CreateAtRandomLocation()
        {
            Point location;
            do
            {
                location = Point.CreateRandom();
            }
            //This is to make sure the new rabbit is not inside the snake or where we write score
            while (SnakeGame.Current.Snake.IsPartOfSnake(location) || location.Y == 0 && location.X <= 10); 

            return new RabbitObj(location); 
        }
        #endregion

        #region Proeprties
        public Point Location { get; set; }
        #endregion
    }
}
