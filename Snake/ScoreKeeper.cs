﻿using System;
using GameEngine;

namespace Snake
{
    public class ScoreKeeper : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(0, 0);
            Console.Write($"Score: {SnakeGame.Score}");
        }
        #endregion
    }
}
