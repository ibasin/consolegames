﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEngine;

namespace Snake
{
    public class SnakeObj : TangibleGameObject
    {
        #region Constructors
        public SnakeObj()
        {
            SnakeDirection = DirectionEnum.Right;
            Body.Add(new Point(Console.WindowWidth / 2, Console.WindowHeight / 2));
            Body.Add(new Point(Console.WindowWidth / 2 - 1, Console.WindowHeight / 2));
            Body.Add(new Point(Console.WindowWidth / 2 - 2, Console.WindowHeight / 2));
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.ReadKey();
            if (key != null)
            {
                if (key.Value.Key == ConsoleKey.UpArrow) SnakeDirection = DirectionEnum.Up;
                if (key.Value.Key == ConsoleKey.DownArrow) SnakeDirection = DirectionEnum.Down;
                if (key.Value.Key == ConsoleKey.RightArrow) SnakeDirection = DirectionEnum.Right;
                if (key.Value.Key == ConsoleKey.LeftArrow) SnakeDirection = DirectionEnum.Left;
            }

            var head = Body.First();
            Point newHead;
            if (SnakeDirection == DirectionEnum.Up) newHead = head.Move(GameEngine.DirectionEnum.Up);
            else if (SnakeDirection == DirectionEnum.Down) newHead = head.Move(GameEngine.DirectionEnum.Down);
            else if (SnakeDirection == DirectionEnum.Right) newHead = head.Move(GameEngine.DirectionEnum.Right);
            else if (SnakeDirection == DirectionEnum.Left) newHead = head.Move(GameEngine.DirectionEnum.Left);
            else throw new Exception("Invalid Direction");

            if (newHead.Violations.IsInViolation) throw new GameOverException($"Game Over. You ran into a wall. Score: {SnakeGame.Score} rabbits.");
            if (IsPartOfSnake(newHead)) throw new GameOverException($"Game Over. You ate yourself. Score: {SnakeGame.Score} rabbits.");

            Body.Insert(0, newHead);

            if (IsPartOfSnake(SnakeGame.Current.Rabbit.Location))
            {
                DigestiveCount += 6;
                SnakeGame.Current.Rabbit = RabbitObj.CreateAtRandomLocation();
                SnakeGame.Score++;
            }

            if (DigestiveCount > 0) DigestiveCount--;
            else Body.RemoveAt(Body.Count - 1);
        }
        public override void Draw()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Body.First().SetCursorPosition();
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Black;
        }
        public override void Erase()
        {
            Body.Last().SetCursorPosition();
            Console.Write(" ");
        }
        public bool IsPartOfSnake(Point point)
        {
            return Body.Any(p => p.X == point.X && p.Y == point.Y);
        }
        #endregion

        #region Properties
        public DirectionEnum SnakeDirection { get; set; }
        public List<Point> Body { get; set; } = new List<Point>();

        public int DigestiveCount { get; set; }
        #endregion
    }
}
