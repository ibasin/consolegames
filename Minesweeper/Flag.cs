﻿using System;
using GameEngine;

namespace Minesweeper
{
    class Flag : TangibleGameObject
    {
        #region Constructors
        public Flag(Point location)
        {
            Location = location;
            
            if (MinesweeperGame.NumberOfFlagsPlaced >= MinesweeperGame.ListOfFlags.Length) return;
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), 'F', ConsoleColor.Red, MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].BgColor));
            MinesweeperGame.NumberOfFlagsPlaced++;
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            if (FirstDraw)
            {
                FlagSprite.Draw(Location);
                FirstDraw = false;
            }
        }

        public override void Erase()
        {
            if (ToDelete) FlagSprite.Erase(Location, MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].BgColor);
        }

        public override void Update()
        {

        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite FlagSprite { get; set; } = new Sprite();
        public bool FirstDraw { get; set; } = true;
        #endregion
    }
}