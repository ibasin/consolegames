﻿using System;
using GameEngine;

namespace Minesweeper
{
    class Plot : TangibleGameObject
    {
        #region Constructors
        public Plot(Point location)
        {
            Location = location;

            //Base Color Assignment
            if (Location.X % 4 == 2 && Location.Y % 2 == 0 || Location.X % 4 == 0 && Location.Y % 2 == 1)
            {
                PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.DarkGreen));
                PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.DarkGreen));
                BgColor = ConsoleColor.DarkGreen;
            }
            else
            {
                PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Black, ConsoleColor.Green));
                PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Green));
                BgColor = ConsoleColor.Green;
            }
        }
    #endregion

        #region Overrides
        public override void Draw()
        {
            if (NewOrJustDug)
            {
                PlotSprite.Draw(Location);
                NewOrJustDug = false;
            }
        }
        public void SetUpPlotChange()
        {
            PlotSprite.PointsWithCharAndColor.RemoveRange(0,2);
            string numOfMinesSurrounding = NumOfMinesSurrounding.ToString();
            char[] character = numOfMinesSurrounding.ToCharArray();
            if (character[0] != '0')
            {
                if (Location.X % 4 == 2 && Location.Y % 2 == 0 || Location.X % 4 == 0 && Location.Y % 2 == 1)
                {
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Yellow, ConsoleColor.Yellow));
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), character[0], ConsoleColor.Black, ConsoleColor.Yellow));
                    BgColor = ConsoleColor.Yellow;
                }
                else
                {
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Yellow, ConsoleColor.White));
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), character[0], ConsoleColor.Black, ConsoleColor.White));
                    BgColor = ConsoleColor.White;
                }
            }
            else
            {
                if (Location.X % 4 == 2 && Location.Y % 2 == 0 || Location.X % 4 == 0 && Location.Y % 2 == 1)
                {
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Yellow, ConsoleColor.Yellow));
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.Yellow));
                    BgColor = ConsoleColor.Yellow;
                }
                else
                {
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, ' ', ConsoleColor.Yellow, ConsoleColor.White));
                    PlotSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Black, ConsoleColor.White));
                    BgColor = ConsoleColor.White;
                }
            }
        }
        public override void Erase()
        {
            if (ToDelete) PlotSprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            if (IsDug && NumOfMinesSurrounding == 0 && !IsZeroMineChecked)
            {
                if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 0 &&
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 0)
                {
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 19 &&
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 19)
                {
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 0 &&
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 19)
                {
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 19 &&
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 0)
                {
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 0)
                {
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 0)
                {
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y + 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.X == 19)
                {
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y + 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y + 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2), Location.Y + 1].IsDug = true;
                    }
                }
                else if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].Location.Y == 19)
                {
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y - 1].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) - 1, Location.Y].IsDug = true;
                    }
                    if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsMine)
                    {
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].SetUpPlotChange();
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].NewOrJustDug = true;
                        MinesweeperGame.GirdOfPlots[(Location.X / 2) + 1, Location.Y].IsDug = true;
                    }
                }
                else
                {
                    for (var x1 = -1; x1 < 2; x1++)
                    {
                        for (var y1 = -1; y1 < 2; y1++)
                        {
                            if (!MinesweeperGame.GirdOfPlots[(Location.X / 2) + x1, Location.Y + y1].IsMine)
                            {
                                MinesweeperGame.GirdOfPlots[(Location.X / 2) + x1, Location.Y + y1].SetUpPlotChange();
                                MinesweeperGame.GirdOfPlots[(Location.X / 2) + x1, Location.Y + y1].NewOrJustDug = true;
                                MinesweeperGame.GirdOfPlots[(Location.X / 2) + x1, Location.Y + y1].IsDug = true;
                            }
                        }
                    }
                }
                IsZeroMineChecked = true;
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite PlotSprite { get; set; } = new Sprite();
        public bool IsMine { get; set; }
        public bool IsMineAllowed { get; set; } = true;
        public bool IsDug { get; set; }
        public bool IsZeroMineChecked { get; set; } = false;
        public bool NewOrJustDug { get; set; } = true;
        public int NumOfMinesSurrounding { get; set; }
        public ConsoleColor BgColor { get; set; }
        #endregion
    }
}