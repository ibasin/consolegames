﻿using System;
using GameEngine;

namespace Minesweeper
{
    class Cursor : TangibleGameObject
    {
        #region Constructors
        public Cursor()
        {
            Location = Point.Zero;

            CursorSprite = new Sprite();
            CursorSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, 'C', ConsoleColor.White, ConsoleColor.Green));
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            CursorSprite.PointsWithCharAndColor.RemoveAt(0);
            if (!MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].IsDug)
            {
                CursorSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, 'C', ConsoleColor.White, MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].BgColor));
            }
            else
            {
                CursorSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, 'C', ConsoleColor.Black, MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].BgColor));
            }
            CursorSprite.Draw(Location);
        }

        public override void Erase()
        {
            CursorSprite.Erase(Location, MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].BgColor);
        }

        public override void Update()
        {
            var key = MinesweeperGame.KeyboardManager.PeekKey();
            if (!key.HasValue) return;


            if (key.Value.Key == ConsoleKey.LeftArrow && Location.X != 0) Location = Location.Move(DirectionEnum.Left, 2);
            if (key.Value.Key == ConsoleKey.RightArrow && Location.X != 38) Location = Location.Move(DirectionEnum.Right, 2);
            if (key.Value.Key == ConsoleKey.UpArrow && Location.Y != 0) Location = Location.Move(DirectionEnum.Up);
            if (key.Value.Key == ConsoleKey.DownArrow && Location.Y != 19) Location = Location.Move(DirectionEnum.Down);

            if (key.Value.Key == ConsoleKey.F && MinesweeperGame.NumberOfFlagsPlaced < MinesweeperGame.ListOfFlags.Length)
            {
                var flag = new Flag(Location);
                MinesweeperGame.Current.GameObjects.Add(flag);
                MinesweeperGame.ListOfFlags[MinesweeperGame.NumberOfFlagsPlaced - 1] = flag;
            }
            if (key.Value.Key == ConsoleKey.R && MinesweeperGame.NumberOfFlagsPlaced != 0)
            {
                for (var i = 0; i < MinesweeperGame.NumberOfFlagsPlaced; i++)
                {
                    if (Location == MinesweeperGame.ListOfFlags[i].Location)
                    {
                        MinesweeperGame.ListOfFlags[i].ToDelete = true;
                        MinesweeperGame.ListOfFlags[i] = null;
                        MinesweeperGame.NumberOfFlagsPlaced--;
                        for (var j = 0; j < MinesweeperGame.NumberOfFlagsPlaced - i - 1; j++)
                        {
                            if (i + j < MinesweeperGame.ListOfFlags.Length)
                            {
                                MinesweeperGame.ListOfFlags[i + j] = MinesweeperGame.ListOfFlags[i + j + 1];
                            }
                            else
                            {
                                MinesweeperGame.ListOfFlags[i + j] = null;
                            }
                        }
                        break;
                    }
                }
            }

            var isFlagAtDigLocation = false;
            for (var i = 0; i < MinesweeperGame.NumberOfFlagsPlaced; i++)
            { 
                if (MinesweeperGame.ListOfFlags[i].Location.X == Location.X && MinesweeperGame.ListOfFlags[i].Location.Y == Location.Y)
                {
                    isFlagAtDigLocation = true;
                    break;
                }
            }
            if (key.Value.Key == ConsoleKey.D && !isFlagAtDigLocation)
            {
                if (!MinesweeperGame.GameBegun)
                {
                    MinesweeperGame.MineSetUp(Location);
                    MinesweeperGame.GameBegun = true;
                }

                if (MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].IsMine)
                {
                    throw new GameOverException("You blew up a mine. Game Over.");
                }
                else
                {
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].SetUpPlotChange();
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].NewOrJustDug = true;
                    MinesweeperGame.GirdOfPlots[Location.X / 2, Location.Y].IsDug = true;
                }
                if (MinesweeperGame.GameEndingCheck())
                {
                    throw new GameOverException("Congratulations. You won.");
                }
            }
            MinesweeperGame.KeyboardManager.ReadKey();
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        public Sprite CursorSprite { get; set; }
        #endregion
    }
}
