﻿using System;
using GameEngine;

namespace Minesweeper
{
    class Score : TangibleGameObject
    {
        #region Constructors
        public Score()
        {
            Location = new Point(0,20);

            int numberOfFlagsLeft = 50 - MinesweeperGame.NumberOfFlagsPlaced;
            int tensPlace = numberOfFlagsLeft / 10;
            int onesPlace = numberOfFlagsLeft % 10;
            string tensPlaceString = tensPlace.ToString();
            string onesPlaceString = onesPlace.ToString();
            char[] tensPlaceChar = tensPlaceString.ToCharArray();
            char[] onesPlaceChar = onesPlaceString.ToCharArray();
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, 'N', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), 'u', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), 'm', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 0), 'b', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 0), 'e', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), 'r', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(6, 0), ' ', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(7, 0), 'o', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(8, 0), 'f', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(9, 0), ' ', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(10, 0), 'f', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(11, 0), 'l', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(12, 0), 'a', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(13, 0), 'g', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(14, 0), 's', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(15, 0), ' ', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(16, 0), 'l', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(17, 0), 'e', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(18, 0), 'f', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(19, 0), 't', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(20, 0), ':', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(21, 0), ' ', ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(22, 0), tensPlaceChar[0], ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(23, 0), onesPlaceChar[0], ConsoleColor.White));

        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            FlagSprite.PointsWithCharAndColor.RemoveRange(22, 2);

            int numberOfFlagsLeft = MinesweeperGame.ListOfFlags.Length - MinesweeperGame.NumberOfFlagsPlaced;
            int tensPlace = numberOfFlagsLeft / 10;
            int onesPlace = numberOfFlagsLeft % 10;
            string tensPlaceString = tensPlace.ToString();
            string onesPlaceString = onesPlace.ToString();
            char[] tensPlaceChar = tensPlaceString.ToCharArray();
            char[] onesPlaceChar = onesPlaceString.ToCharArray();
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(22, 0), tensPlaceChar[0], ConsoleColor.White));
            FlagSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(23, 0), onesPlaceChar[0], ConsoleColor.White));
            FlagSprite.Draw(Location);
        }
        public override void Erase()
        {

        }

        public override void Update()
        {

        }
        #endregion

        #region Properties
        public Sprite FlagSprite { get; set; } = new Sprite();
        public Point Location { get; set; }
        #endregion
    }

}