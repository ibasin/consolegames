﻿using GameEngine;

namespace Minesweeper
{
    class MinesweeperGame : Game
    {
        #region Constructors
        public MinesweeperGame()
        {
            Current = this;

            var cursor = new Cursor();
            GameObjects.Add(cursor);

            var score = new Score();
            GameObjects.Add(score);

            for (var x = 0; x < 40; x += 2)
            {
                for (var y = 0; y < 20; y++)
                {
                    var plot = new Plot(new Point(x, y));
                    GameObjects.Add(plot);
                    GirdOfPlots[x / 2, y] = plot;
                }
            }
        }
        #endregion

        #region Methods
        public static void MineSetUp(Point firstDigLocation)
        {
            #region MineSetUp
            {
                if (firstDigLocation.X == 0 && firstDigLocation.Y == 0)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.X == 19 && firstDigLocation.Y == 19)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.X == 19 && firstDigLocation.Y == 0)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.X == 0 && firstDigLocation.Y == 19)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.X == 0)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.Y == 0)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.X == 19)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsMineAllowed = false;
                }
                else if (firstDigLocation.Y == 19)
                {
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsMineAllowed = false;
                    GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsMineAllowed = false;
                }
                else
                {
                    for (var x = -1; x < 2; x++)
                    {
                        for (var y = -1; y < 2; y++)
                        {
                            GirdOfPlots[(firstDigLocation.X / 2) + x, firstDigLocation.Y + y].IsMineAllowed = false;
                        }
                    }
                }

            }
            #endregion

            //Mine Assignment
            for (var i = 0; i < ListOfFlags.Length; i++)
            {
                while (true)
                {
                    int randomX = Rnd.Next(0, 20);
                    int randomY = Rnd.Next(0, 20);
                    if (GirdOfPlots[randomX, randomY].IsMineAllowed)
                    {
                        GirdOfPlots[randomX, randomY].IsMine = true;
                        GirdOfPlots[randomX, randomY].IsMineAllowed = false;
                        break;
                    }
                }
            }

            #region MineCounting
            for (var x = 0; x < 20; x++)
            {
                for (var y = 0; y < 20; y++)
                {
                    for (var x1 = -1; x1 < 2; x1++)
                    {
                        for (var y1 = -1; y1 < 2; y1++)
                        {
                            var plot = GirdOfPlotsSafeGet(x + x1, y + y1);
                            if (plot != null && plot.IsMine)
                            {
                                GirdOfPlots[x, y].NumOfMinesSurrounding++;
                            }
                        }
                    }
                }
            }
            #endregion

            #region MineDigging
            if (firstDigLocation.X == 0 && firstDigLocation.Y == 0)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsDug = true;
            }
            else if (firstDigLocation.X == 19 && firstDigLocation.Y == 19)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsDug = true;
            }
            else if (firstDigLocation.X == 19 && firstDigLocation.Y == 0)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsDug = true;
            }
            else if (firstDigLocation.X == 0 && firstDigLocation.Y == 19)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsDug = true;
            }
            else if (firstDigLocation.X == 0)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsDug = true;


                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsDug = true;
            }
            else if (firstDigLocation.Y == 0)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y + 1].IsDug = true;
            }
            else if (firstDigLocation.X == 19)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y + 1].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y + 1].IsDug = true;
            }
            else if (firstDigLocation.Y == 19)
            {
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[firstDigLocation.X / 2, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y - 1].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) - 1, firstDigLocation.Y].IsDug = true;

                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].SetUpPlotChange();
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].NewOrJustDug = true;
                GirdOfPlots[(firstDigLocation.X / 2) + 1, firstDigLocation.Y].IsDug = true;
            }
            else
            {
                for (var x = -1; x < 2; x++)
                {
                    for (var y = -1; y < 2; y++)
                    {
                        GirdOfPlots[(firstDigLocation.X / 2) + x, firstDigLocation.Y].SetUpPlotChange();
                        GirdOfPlots[(firstDigLocation.X / 2) + x, firstDigLocation.Y].NewOrJustDug = true;
                        GirdOfPlots[(firstDigLocation.X / 2) + x, firstDigLocation.Y].IsDug = true;
                    }
                }
            }
            #endregion
        }
        public static bool GameEndingCheck()
        {
            var numberOfPlacesDug = 0;
            for (var i = 0; i < 20; i++)
            {
                for (var j = 0; j < 20; j++)
                {
                    if (GirdOfPlots[i, j].IsDug) numberOfPlacesDug++;
                }
            }
            if (numberOfPlacesDug == (20 * 20) - ListOfFlags.Length) return true;
            return false;
        }
        #endregion

        #region PrivateHelpers
        public static Plot GirdOfPlotsSafeGet(int x, int y)
        {
            if (x < 0) return null;
            if (y < 0) return null;
            if (x >= GirdOfPlots.GetLength(0)) return null;
            if (y >= GirdOfPlots.GetLength(1)) return null;

            return GirdOfPlots[x, y];
        }
        #endregion

        #region Properties
        public static MinesweeperGame Current { get; private set; }
        public static int NumberOfFlagsPlaced { get; set; }
        public static Plot[,] GirdOfPlots { get; } = new Plot[20, 20];
        public static Flag[] ListOfFlags { get; } = new Flag[70];
        public static bool GameBegun { get; set; }
        #endregion
    }
}