﻿using GameEngine;

namespace Duel
{
    public class LeftFlyingBullet : Bullet
    {
        #region Constructors
        public LeftFlyingBullet(Point location) : base(location) { }
        #endregion

        #region Overrides
        public override void Update()
        {
            double ChanceOfFall = 0.015;
            bool theBulletHasFallen = false;
            double bulletFall = Game.Rnd.NextDouble();

            if (bulletFall > ChanceOfFall && theBulletHasFallen == false)
            {
                Location = Location.Move(DirectionEnum.Right, 2);
            }
            else
            {
                Location = Location.Move(DirectionEnum.Down);
                Location = Location.Move(DirectionEnum.Right);
                theBulletHasFallen = true;
            }

            if(Body.IsCollidingAtLocation(Location, DuelGame.Current.Player2.Body, DuelGame.Current.Player2.Location))
            {
                throw new GameOverException("Republicans win!");
            }
        }
        #endregion
    }
}
