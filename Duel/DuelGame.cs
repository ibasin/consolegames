﻿using System;
using GameEngine;

namespace Duel
{
    public class DuelGame : Game
    {
        #region Constructors
        public DuelGame()
        {
            Player1 = new LeftPlayer(new Point(-3, Console.WindowHeight - 14));
            Player2 = new RightPlayer(new Point(Console.WindowWidth - 1,Console.WindowHeight - 14));
            GameObjects.Add(Player1);
            GameObjects.Add(Player2);

            Primitives.DrawHorizontalLine(Console.WindowHeight - 9, 0, Console.WindowWidth - 1, 'T', ConsoleColor.White);
            Primitives.DrawHorizontalLine(Console.WindowHeight - 10, 0, Console.WindowWidth - 1, 'T', ConsoleColor.White);
            Primitives.DrawHorizontalLine(Console.WindowHeight - 11, 0, Console.WindowWidth - 1, 'T', ConsoleColor.Yellow);

            Current = this;
        }
        #endregion

        #region Properties
        public static DuelGame Current { get; private set; }
        public LeftPlayer Player1 { get; }
        public RightPlayer Player2 { get; }
        #endregion
    }
}