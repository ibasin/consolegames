﻿using System;
using GameEngine;

namespace Duel
{
    class RightFlyingBullet : Bullet
    {
        #region Constructors
        public RightFlyingBullet(Point location) : base(location) { }
        #endregion

        #region Overrides
        public override void Update()
        {
            double ChanceOfFall = 0.015;
            bool theBulletHasFallen = false;
            double bulletFall = Game.Rnd.NextDouble();

            if (bulletFall > ChanceOfFall && theBulletHasFallen == false)
            {
                Location = Location.Move(DirectionEnum.Left, 2);
            }
            else
            {
                Location = Location.Move(DirectionEnum.Down);
                Location = Location.Move(DirectionEnum.Left);
                theBulletHasFallen = true;
            }

            if (Body.IsCollidingAtLocation(Location, DuelGame.Current.Player1.Body, DuelGame.Current.Player1.Location))
            {
                throw new GameOverException("Democrats win!");
            }
        }
        #endregion
    }
}
