﻿using System;
using GameEngine;

namespace Duel
{
    public abstract  class Bullet : TangibleGameObject
    {
        #region Constructors
        public Bullet(Point location)
        {
            Location = location;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(Point.Zero, '=', ConsoleColor.Cyan));
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }
        #endregion
    }
}
