﻿using System;
using GameEngine;

namespace Duel
{
        public class RightPlayer : TangibleGameObject
        {
            #region Constructors
            public RightPlayer(Point location)
            {
                #region Sprite
                Body = new Sprite();

                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 0), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 0), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 2), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, -1), ' ', ConsoleColor.Blue, ConsoleColor.Blue));
                #endregion

                #region Location
                Location = location;
                #endregion
            }
            #endregion

            #region Overrides
            public override void Update()
            {
                
                var newLocation = Location.Move(DirectionEnum.Left);
                if (newLocation.X >= Console.WindowWidth * 2 / 3) Location = newLocation;
                

                var key = Game.KeyboardManager.PeekKey();
                if (key != null && key.Value.Key == ConsoleKey.L && NumberOfBullets == 0)
                {
                    var bullet = new RightFlyingBullet(Location);
                    DuelGame.Current.GameObjects.Add(bullet);

                    NumberOfBullets = 1;
                }
            }

            public override void Draw()
            {
                Body.Draw(Location);
            }

            public override void Erase()
            {
                Body.Erase(Location, ConsoleColor.Black);
            }
            #endregion

            #region Properties
            public Point Location { get; protected set; }
            public Sprite Body { get; }
            protected int NumberOfBullets { get; set; }
            #endregion
        }
    }

   