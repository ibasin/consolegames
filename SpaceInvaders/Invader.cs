﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class Invader : TangibleOneSpaceGameObject
    {
        #region Constructors
        public Invader(Point location) : base(location){}
        #endregion

        #region Methods
        public override void Update()
        {
            Counter++;
            if (Counter % 3 == 0)
            {
                var randomizer = Game.Rnd.NextDouble();

                if (randomizer < 0.05) SpaceInvadersGame.Current.GameObjects.Add(new Bullet(Location.Move(DirectionEnum.Down), false, ConsoleColor.Red));
                else if (randomizer < 0.35) Location = Location.Move(DirectionEnum.Left);
                else if (randomizer < 0.65) Location = Location.Move(DirectionEnum.Right);
                else if (randomizer < 0.9) Location = Location.Move(DirectionEnum.Down);
                else Location = Location.Move(DirectionEnum.Up);

                if (Location.Violations.XPlusViolation)
                {
                    throw new GameOverException($"Game Over. Invaders got through! You killed {SpaceInvadersGame.InvadersKilled} invader(s).");
                }

                if (Location.Violations.IsInViolation) Location = Location.CorrectViolations();
            }
        }
        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("M");
        }
        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.Write(" ");
        }
        #endregion

        #region Proeprties
        protected int Counter { get; set; }
        public override bool ToDelete
        {
            get => base.ToDelete;
            set
            {
                if (value == base.ToDelete) return;
                if (value) SpaceInvadersGame.InvadersKilled++;
                base.ToDelete = value;
            }
        }
        #endregion
    }
}
