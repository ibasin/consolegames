﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class SpaceInvadersGame : Game
    {
        #region Constructors
        public SpaceInvadersGame()
        {
            Current = this;
            GameObjects.Add(new ScoreKeeper());
            GameObjects.Add(new InvaderGenerator());
            GameObjects.Add(new SpaceShip(new Point(Console.WindowWidth / 2, Console.WindowHeight - 2)));
        }
        #endregion

        #region Properties
        public static SpaceInvadersGame Current { get; protected set; }
        public static int InvadersKilled { get; set; }
        #endregion
    }
}
