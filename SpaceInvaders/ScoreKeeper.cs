﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class ScoreKeeper : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.SetCursorPosition(0, 0);
            Console.Write($"Score: {SpaceInvadersGame.InvadersKilled}");
        }
        #endregion
    }
}
