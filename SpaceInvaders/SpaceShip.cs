﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class SpaceShip : TangibleOneSpaceGameObject
    {
        #region Constructors
        public SpaceShip(Point location) : base(location) { }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.ReadKey();
            if (key != null)
            {
                if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right);
                if (key.Value.Key == ConsoleKey.LeftArrow) Location = Location.Move(DirectionEnum.Left);
                if (key.Value.Key == ConsoleKey.DownArrow) Location = Location.Move(DirectionEnum.Down);
                if (key.Value.Key == ConsoleKey.UpArrow) Location = Location.Move(DirectionEnum.Up);
                if (key.Value.Key == ConsoleKey.Spacebar) SpaceInvadersGame.Current.GameObjects.Add(new Bullet(Location.Move(DirectionEnum.Up), true, ConsoleColor.Cyan));
            }

            if (Location.Violations.IsInViolation) Location = Location.CorrectViolations();
        }
        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("W");
        }
        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.Write(" ");
        }
        public override bool ToDelete
        {
            get => base.ToDelete;
            set
            {
                if (value == base.ToDelete) return;
                if (value) throw new GameOverException($"GameOver. Invaders killed you. You killed {SpaceInvadersGame.InvadersKilled} invader(s).");
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                base.ToDelete = value;
            }
        }
        #endregion
    }
}
