﻿using System.Linq;
using GameEngine;

namespace SpaceInvaders
{
    public abstract class TangibleOneSpaceGameObject : TangibleGameObject
    {
        #region Constructors
        protected TangibleOneSpaceGameObject(Point location)
        {
            Location = location;
        }
        #endregion

        #region Methods
        public override void PostUpdate()
        {
            if (ToDelete) return;

            var kill = SpaceInvadersGame.Current.GameObjects
                .Where(o => o is TangibleOneSpaceGameObject)
                .FirstOrDefault(o => ((TangibleOneSpaceGameObject)o).Location == Location && !ReferenceEquals(o, this));

            if (kill != null)
            {
                ToDelete = true;
                kill.ToDelete = true;
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; }
        #endregion
    }
}
