﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class InvaderGenerator : GameObject
    {
        #region Overrides
        public override void Update()
        {
            if (Game.Rnd.NextDouble() < 0.05)
            {
                SpaceInvadersGame.Current.GameObjects.Add(new Invader(new Point(Game.Rnd.Next(Console.WindowWidth - 1), 0)));
            }
        }
        #endregion
    }
}
