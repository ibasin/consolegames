﻿using System;
using GameEngine;

namespace SpaceInvaders
{
    public class Bullet : TangibleOneSpaceGameObject
    {
        #region Constructors
        public Bullet(Point location, bool goingUp, ConsoleColor color) : base(location)
        {
            if (goingUp) Speed = -1;
            else Speed = 1;
            Color = color;
        }
        #endregion

        #region Methods
        public override void Update()
        {
            if (Location.Y == 0 || Location.Y >= Console.WindowHeight - 1)
            {
                ToDelete = true;
                return;
            }
            Location = Location.Move(DirectionEnum.Down, Speed);

            if (Location.Violations.IsInViolation) Location = Location.CorrectViolations();
        }
        public override void Draw()
        {
            Location.SetCursorPosition();
            Console.ForegroundColor = Color;
            Console.Write(".");
        }
        public override void Erase()
        {
            Location.SetCursorPosition();
            Console.Write(" ");
        }
        #endregion

        #region Properties
        protected int Speed { get; set; }
        protected ConsoleColor Color { get; set; }
        #endregion
    }
}
