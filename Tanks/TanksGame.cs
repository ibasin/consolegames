﻿using System;
using GameEngine;

namespace Tanks
{
    public class TanksGame : Game
    {
        #region Constructors
        public TanksGame()
        {
            IterationSleep = 200;
            GameObjects.Add(new Tank(new Point(Console.WindowWidth / 2 - 8 / 2, Console.WindowHeight - Tank.TankHeight - 1)));
            GameObjects.Add(new Plane());
            Primitives.DrawHorizontalLine(Console.WindowHeight - 1, 0, Console.WindowWidth - 1, '~', ConsoleColor.Green);
            Current = this;
        }
        #endregion

        #region Properties
        public static TanksGame Current { get; private set; }
        #endregion
    }
}
