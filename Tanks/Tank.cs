﻿using System;
using GameEngine;

namespace Tanks
{
    public class Tank : TangibleGameObject
    {
        #region Constructors
        public Tank(Point location)
        {
            #region Sprite
            Body = new Sprite();

            //Turret
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2,0), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3,0), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4,0), ' ', ConsoleColor.DarkGreen, ConsoleColor.DarkGreen));

            //Gun
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5,0), '-', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(6,0), '-', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(7,0), '-', ConsoleColor.DarkGreen));

            //Body
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0,1), '=', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1,1), '=', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2,1), '=', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3,1), '=', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4,1), '=', ConsoleColor.DarkGreen));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5,1), '=', ConsoleColor.DarkGreen));

            //Wheels
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0,2), '@', ConsoleColor.DarkGray));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1,2), '@', ConsoleColor.DarkGray));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2,2), '@', ConsoleColor.DarkGray));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3,2), '@', ConsoleColor.DarkGray));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4,2), '@', ConsoleColor.DarkGray));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5,2), '@', ConsoleColor.DarkGray));

            if (Body.Size.Y != TankHeight) throw new Exception("MySprite.Size.Y != TankHeight");
            #endregion

            #region Location
            Location = location;
            #endregion
        }
        #endregion
        
        #region Overrides
        public override void Update()
        {
            var location = new Point(Location);
            
            while(true)
            {
                var key = TanksGame.KeyboardManager.ReadKey();
                
                if (!key.HasValue) break;

                if (key.Value.Key == ConsoleKey.UpArrow && CoolDownCounter == 0)
                {
                    var bullet = new Bullet(Location.Move(DirectionEnum.Right, 8));
                    TanksGame.Current.GameObjects.Add(bullet);
                    CoolDownCounter = 5;
                }

                if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right);
                if (key.Value.Key == ConsoleKey.LeftArrow) 
                {
                    Location = Location.Move(DirectionEnum.Left);
                    // ReSharper disable once EmptyEmbeddedStatement
                    while(TanksGame.KeyboardManager.ReadKey().HasValue);
                }
            }

            if (!Body.IsFullyValidAtLocation(Location)) Location = location;

            CoolDownCounter--;
            if (CoolDownCounter < 0) CoolDownCounter = 0;
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }

        protected int CoolDownCounter { get; set; }
        
        public const int TankHeight = 3;
        #endregion
    }
}
