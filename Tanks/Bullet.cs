﻿using System;
using GameEngine;

namespace Tanks
{
    public class Bullet : TangibleGameObject
    {
        #region Constructors
        public Bullet(Point location)
        {
            Location = location;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0,0), '-', ConsoleColor.Red));
        }
        #endregion
        
        #region Overrides
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Right, 5);
            if (!Body.IsFullyValidAtLocation(Location)) ToDelete = true;
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }
        #endregion
    }
}
