﻿using System;
using System.Runtime.InteropServices;
using GameEngine;

namespace AsteroidDodge
{
    public class ShipObj : TangibleGameObject
    {
        #region Constructors
        public ShipObj(Point location)
        {
            #region Sprite
            MySprite = new Sprite();

            //Body
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.White, ConsoleColor.White));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.White, ConsoleColor.White));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', ConsoleColor.White, ConsoleColor.White));

            //Top finn
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '^', ConsoleColor.Red));

            //Side finn
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '-', ConsoleColor.Red, ConsoleColor.White));
           
            //Point
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '>', ConsoleColor.Red));
            #endregion

            #region Location
            Location = location;
            #endregion
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var location = new Point(Location);

            while (true)
            {
                var key = ShipsGame.KeyboardManager.ReadKey();

                if (!key.HasValue) break;

                if (key.Value.Key == ConsoleKey.UpArrow) Location = Location.Move(DirectionEnum.Up);
                if (key.Value.Key == ConsoleKey.DownArrow) Location = Location.Move(DirectionEnum.Down);
            }

            if (!MySprite.IsFullyValidAtLocation(Location)) Location = location;
        }
        public override void Draw()
        {
            MySprite.Draw(Location);
        }
        public override void Erase()
        {
            MySprite.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite MySprite { get; }
        #endregion
    }
}
