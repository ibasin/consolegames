﻿using System;
using GameEngine;

namespace AsteroidDodge
{
    public class Score : TangibleGameObject
    {
        #region Overrides
        public override void Erase()
        {
        }

        public override void Update()
        {
            Iteration++;
        }

        public override void Draw()
        {
            Point.Zero.SetCursorPosition();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write($"Score: {Iteration}");
        }
        #endregion

        #region Properties
        public int Iteration { get; set; }
        #endregion
    }
}
