﻿using System;
using GameEngine;

namespace AsteroidDodge
{
    public class AsteroidObj : TangibleGameObject
    {
        #region Constructors
        public AsteroidObj(Point location) 
        {
            Location = location;
            MySprite = new Sprite();
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', ConsoleColor.Black, ConsoleColor.Gray));
        }
        #endregion

        #region Methods
        public override void Draw()
        {
            MySprite.Draw(Location);
        }
        public override void Erase()
        {
            MySprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Left);
            if (MySprite.IsFullyValidAtLocation(location)) Location = location;
            else ToDelete = true;

            var ship = AsteroidDodgeGame.Current.ShipObj;
            if (MySprite.IsCollidingAtLocation(Location, ship.MySprite, ship.Location)) throw new GameOverException($"You really suck. Your score was only {AsteroidDodgeGame.Current.Score.Iteration}. Learn to play.");
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        //public ShipObj Ship { get; }
        protected Sprite MySprite { get; }
        #endregion
    }
}
