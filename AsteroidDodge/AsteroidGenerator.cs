﻿using System;
using GameEngine;

namespace AsteroidDodge
{
    public class AsteroidGenerator : GameObject
    {
        #region Overrides
        public override void Update()
        {
            if (Game.Rnd.NextDouble() < AsteroidDodgeGame.Current.Score.Iteration / 500.0)
            {
                var location = new Point(Console.WindowWidth - 3, Game.Rnd.Next(Console.WindowHeight));

                AsteroidDodgeGame.Current.GameObjects.Add(new AsteroidObj(location));
            }
        }
        #endregion
    }
}