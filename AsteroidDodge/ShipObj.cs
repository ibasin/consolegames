﻿using System;
using GameEngine;

namespace AsteroidDodge
{
    public class ShipObj : TangibleGameObject
    {
        #region Constructors
        public ShipObj(Point location)
        {
            #region Sprite
            MySprite = new Sprite();

            //Body
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.White, ConsoleColor.White));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.White, ConsoleColor.White));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', ConsoleColor.White, ConsoleColor.White));

            //Top finn
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '^', ConsoleColor.Red));

            //Side finn
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '-', ConsoleColor.Red, ConsoleColor.White));

            //Point
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '>', ConsoleColor.Red));
            #endregion

            #region Location
            Location = location;
            #endregion
        }
        #endregion

        #region Overrides
        public override void Erase()
        {
            var key = AsteroidDodgeGame.KeyboardManager.PeekKey();
            if (key.HasValue)
            {
                if (key.Value.Key == ConsoleKey.UpArrow) ShipMovedThisIteration = true;
                if (key.Value.Key == ConsoleKey.DownArrow) ShipMovedThisIteration = true;
            }
            else
            {
                ShipMovedThisIteration = false;
            }

            if (ShipMovedThisIteration) MySprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = new Point(Location);

            while (true)
            {
                var key = AsteroidDodgeGame.KeyboardManager.ReadKey();

                if (!key.HasValue) break;

                if (key.Value.Key == ConsoleKey.UpArrow) Location = Location.Move(DirectionEnum.Up);
                if (key.Value.Key == ConsoleKey.DownArrow) Location = Location.Move(DirectionEnum.Down);
            }

            if (!MySprite.IsFullyValidAtLocation(Location)) Location = location;
        }
        public override void Draw()
        {
            if (ShipMovedThisIteration || FirstIteration)
            {
                MySprite.Draw(Location);
                FirstIteration = false;
            }
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite MySprite { get; }
        protected bool ShipMovedThisIteration { get; set; }
        protected bool FirstIteration { get; set; } = true;
        #endregion
    }
}