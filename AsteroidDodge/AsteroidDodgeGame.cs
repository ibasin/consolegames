﻿using System;
using GameEngine;

namespace AsteroidDodge
{
    public class AsteroidDodgeGame : Game
    {
        #region Constructors
        public AsteroidDodgeGame()
        {
            ShipObj = new ShipObj(new Point(Console.WindowWidth / 19 - 16 / 4, Console.WindowHeight - 13));
            GameObjects.Add(ShipObj);

            AsteroidGenerator = new AsteroidGenerator();
            GameObjects.Add(AsteroidGenerator);
            Current = this;

            Score = new Score();
            GameObjects.Add(Score);
        }
        #endregion

        #region Properties
        public static AsteroidDodgeGame Current { get; private set; }
        public ShipObj ShipObj { get; }
        public AsteroidGenerator AsteroidGenerator { get; }
        public Score Score { get; }
        #endregion
    }
}