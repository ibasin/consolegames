﻿using System;
using GameEngine;

namespace TenseCE
{
    class Player : TangibleGameObject
    {
        #region Constructors
        public Player(Point location)
        {
            PlayerSprite = new Sprite();

            PlayerSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', ConsoleColor.White, ConsoleColor.Green));
            PlayerSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.White, ConsoleColor.Green));
            PlayerSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.White, ConsoleColor.Green));
            PlayerSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.White, ConsoleColor.Green));

            Location = location;
        }
        #endregion

        #region Overrides
        public override void Erase()
        {
            var key = Game.KeyboardManager.PeekKey();
            if (key.HasValue)
            {
                if (key.Value.Key == ConsoleKey.LeftArrow) PlayerMoved = true;
                if (key.Value.Key == ConsoleKey.RightArrow) PlayerMoved = true;
            }
            else
            {
                PlayerMoved = false;
            }

            if (PlayerMoved) PlayerSprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = new Point(Location);

            while (true)
            {
                var key = Game.KeyboardManager.ReadKey();

                if (!key.HasValue) break;

                if (key.Value.Key == ConsoleKey.LeftArrow) Location = Location.Move(DirectionEnum.Left);
                if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right);
            }

            if (Location.X > Console.WindowWidth / 2 + 20 || Location.X < Console.WindowWidth / 2 - 20) Location = location;
        }
        public override void Draw()
        {
            PlayerSprite.Draw(Location);
            FirstIteration = false;
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite PlayerSprite { get; }
        protected bool PlayerMoved { get; set; }
        protected bool FirstIteration { get; set; } = true;
        #endregion
    }
}
