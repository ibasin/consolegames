﻿using System;
using GameEngine;

namespace TenseCE
{
    class Enemy : TangibleGameObject
    {
        #region Constructors
        public Enemy(Point location)
        {
            EnemySprite = new Sprite();

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    EnemySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), ' ', ConsoleColor.White, ConsoleColor.Red));
                }
            }

            Location = location;
        }
        #endregion

        #region Methods
        public override void Draw()
        {
            EnemySprite.Draw(Location);
        }
        public override void Erase()
        {
            EnemySprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Down);
            if (EnemySprite.IsFullyValidAtLocation(location)) Location = location;
            else ToDelete = true;

            var player = TenseGame.Current.Player;
            if (EnemySprite.IsCollidingAtLocation(Location, player.PlayerSprite, player.Location)) throw new GameOverException($"GG. You collected {TenseGame.Current.Score.Points} coins. Press enter to continue...");
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite EnemySprite { get; }
        #endregion
    }
}
