﻿using System;
using GameEngine;

namespace TenseCE
{
    class Score : TangibleGameObject
    {
        #region Overrides
        public override void Erase()
        {
        }

        public override void Update()
        {
            Iteration++;
        }

        public override void Draw()
        {
            Console.SetCursorPosition(Position.X, Position.Y);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write($"Score: {Points}");
        }
        #endregion

        #region Properties
        Point Position
        {
            get
            {
                return new Point(Console.WindowWidth / 2, 0);
            }
        }
        public int Points { get; set; }
        public int Iteration { get; set; }
        #endregion
    }
}
