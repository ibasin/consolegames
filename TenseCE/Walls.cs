﻿using System;
using GameEngine;

namespace TenseCE
{
    class Walls : TangibleGameObject
    {
        #region Constructors
        public Walls ()
        {
            WallSprite = new Sprite();

            for (int x = 0; x < 45; x += 44)
            {
                for (int y = 0; y < Console.WindowHeight; y++)
                {
                    WallSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, y), '_', ConsoleColor.Gray, ConsoleColor.DarkGray));
                }
            }
        }
        #endregion

        #region Methods
        public override void Draw()
        {
            WallSprite.Draw(new Point(Console.WindowWidth / 2 - WallSprite.Size.X / 2, 0));
        }
        public override void Erase()
        {
            //Doesn't erase. Ever. It's a wall.
        }
        public override void Update()
        {
            //Once again, this is a wall.
        }
        #endregion

        #region Properties
        protected Sprite WallSprite { get; }
        #endregion
    }
}
