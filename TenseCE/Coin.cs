﻿using System;
using GameEngine;
using System.Speech.Synthesis;

namespace TenseCE
{
    class Coin : TangibleGameObject
    {
        #region Constructors
        public Coin(Point location)
        {
            CoinSprite = new Sprite();

            CoinSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', ConsoleColor.White, ConsoleColor.Yellow));
            CoinSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.White, ConsoleColor.Yellow));
            CoinSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', ConsoleColor.White, ConsoleColor.Yellow));
            CoinSprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', ConsoleColor.White, ConsoleColor.Yellow));

            Location = location;
        }
        #endregion

        #region Methods
        public override void Draw()
        {
            CoinSprite.Draw(Location);
        }
        public override void Erase()
        {
            CoinSprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Down);
            if (CoinSprite.IsFullyValidAtLocation(location)) Location = location;
            else ToDelete = true;

            var player = TenseGame.Current.Player;
            if (CoinSprite.IsCollidingAtLocation(Location, player.PlayerSprite, player.Location))
            {
                TenseGame.Current.Score.Points++;
                ToDelete = true;

                SpeechSynthesizer synthesizer = new SpeechSynthesizer();
                synthesizer.SpeakAsync("Coin");
            }
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite CoinSprite { get; }
        #endregion
    }
}
