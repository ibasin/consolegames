﻿using System;
using GameEngine;

namespace TenseCE
{
    class TenseGame : Game
    {
        #region Constructors
        public TenseGame()
        {
            Player = new Player(new Point(Console.WindowWidth / 2, Console.WindowHeight - 2));
            GameObjects.Add(Player);

            EnemySpawner = new EnemySpawner();
            CoinSpawner = new CoinSpawner();
            Walls = new Walls();
            GameObjects.Add(EnemySpawner);
            GameObjects.Add(CoinSpawner);
            GameObjects.Add(Walls);
            Current = this;

            Score = new Score();
            GameObjects.Add(Score);
        }
        #endregion

        #region Properties
        public static TenseGame Current { get; private set; }
        public Player Player { get; }
        public EnemySpawner EnemySpawner { get; }
        public CoinSpawner CoinSpawner { get; }
        public Walls Walls { get; }
        public Score Score { get; }
        #endregion
    }
}
