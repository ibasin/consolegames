﻿using System;
using GameEngine;

namespace TenseCE
{
    class EnemySpawner : GameObject
    {
        #region Overrides
        public override void Update()
        {
            if (TenseGame.Current.Score.Iteration % 5 == 0)
            {
                var location = new Point(Console.WindowWidth / 2 - TenseGame.Rnd.Next(-20, 20), 1);

                TenseGame.Current.GameObjects.Add(new Enemy(location));
            }
        }
        #endregion
    }
}
