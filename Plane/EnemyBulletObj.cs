﻿using System;
using GameEngine;

namespace Plane
{
    public class EnemyBulletObj : TangibleGameObject
    {
        #region Constructors
        public EnemyBulletObj(Point location)
        {
            Location = location;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, -1), '-', ConsoleColor.Cyan));
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Left, 3);

            if (Body.IsFullyValidAtLocation(location)) Location = location;
            else ToDelete = true;

            var plane = PlaneGame.Current.Plane;
            if (Body.IsCollidingAtLocation(Location, plane.MySprite, plane.Location)) throw new GameOverException("You died!");
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }

        #endregion
    }
}

