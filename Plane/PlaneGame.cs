﻿using GameEngine;
using System;

namespace Plane
{
    public class PlaneGame : Game
    {
        #region Constructors
        public PlaneGame()
        {
            IterationSleep = 200;
            
            Plane = new PlaneObj(new Point(0, Console.WindowHeight/2 - 3/2));
            GameObjects.Add(Plane);

            EnemyPlane = new EnemyObj(new Point(Console.WindowWidth - 6, 0));
            GameObjects.Add(EnemyPlane);

            Current = this;
        }
        #endregion

        #region Properties
        public static PlaneGame Current { get; private set; }
        public PlaneObj Plane { get; }
        public EnemyObj EnemyPlane { get; }
        #endregion
    }

}
