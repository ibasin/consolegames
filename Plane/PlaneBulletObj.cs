﻿using System;
using GameEngine;

namespace Plane
{
        public class PlaneBulletObj : TangibleGameObject
        {
            #region Constructors
            public PlaneBulletObj(Point location)
            {
                Location = location;

                Body = new Sprite();
                Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '-', ConsoleColor.Yellow));
            }
            #endregion

            #region Overrides
            public override void Update()
            {
                Location = Location.Move(DirectionEnum.Right, 3);
                if (!Body.IsFullyValidAtLocation(Location)) ToDelete = true;
            }
            public override void Draw()
            {
                Body.Draw(Location);
            }
            public override void Erase()
            {
                Body.Erase(Location, ConsoleColor.Black);
            }
            #endregion

            #region Properties
            protected Point Location { get; set; }
            protected Sprite Body { get; }
            #endregion
        }
    }
