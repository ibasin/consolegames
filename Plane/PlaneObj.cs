﻿using GameEngine;
using System;

namespace Plane
{
   
    public class PlaneObj : TangibleGameObject
    {
        #region Constructors
        public PlaneObj(Point location)
        {
            #region Sprite
            MySprite = new Sprite();

            //middle
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '>', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '>', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '=', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '=', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '=', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 1), '=', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(6, 1), '=', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(7, 1), '=', ConsoleColor.DarkGray));

            //gun
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(8, 1), '>', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(9, 1), '>', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(10, 1), '>', ConsoleColor.DarkGray));
            #endregion

            #region Location
            Location = location;
            #endregion
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var location = new Point(Location);

            while (true)
            {
                var key = Game.KeyboardManager.ReadKey();

                if (!key.HasValue) break;

                if (key.Value.Key == ConsoleKey.Spacebar && CoolDownCounter == 0)
                {
                    var bullet = new PlaneBulletObj(Location.Move(DirectionEnum.Right, 10));
                    PlaneGame.Current.GameObjects.Add(bullet);
                    CoolDownCounter = 3;
                }

                if (key.Value.Key == ConsoleKey.RightArrow) Location = Location.Move(DirectionEnum.Right);
                if (key.Value.Key == ConsoleKey.LeftArrow) Location = Location.Move(DirectionEnum.Left);

                if (key.Value.Key == ConsoleKey.UpArrow) Location = Location.Move(DirectionEnum.Up);
                if (key.Value.Key == ConsoleKey.DownArrow) Location = Location.Move(DirectionEnum.Down);
            }

            if (!MySprite.IsFullyValidAtLocation(Location)) Location = location;
            CoolDownCounter--;
            if (CoolDownCounter < 0) CoolDownCounter = 0;
        }
        public override void Draw()
        {
            MySprite.Draw(Location);
        }
        public override void Erase()
        {
            MySprite.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite MySprite { get; }
        protected int CoolDownCounter { get; set; }
        #endregion
    }
}
