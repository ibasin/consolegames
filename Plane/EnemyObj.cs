﻿using System;
using GameEngine;

namespace Plane
{
    public class EnemyObj : TangibleGameObject
    {
        #region Constructors
        public EnemyObj(Point point)
        {
            Location = point;

            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 0), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '@', ConsoleColor.Magenta));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '@', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '.', ConsoleColor.Yellow));
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Down, 2);
            if (!Body.IsFullyValidAtLocation(Location)) Location = new Point(Console.WindowWidth - 6, 0);
            
            if (Game.Rnd.Next(5) == 4)
            {
                var bullet = new EnemyBulletObj(Location.Move(DirectionEnum.Left));
                PlaneGame.Current.GameObjects.Add(bullet);
            }
        }
     
        public override void Draw()
        {
            Body.Draw(Location);
        }
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Properties
        protected Point Location { get;  set; }
        protected Sprite Body { get; }
        #endregion
    }
}


