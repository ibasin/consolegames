﻿using System;
using GameEngine;

namespace Tetris
{
    public static class Data
    {
        #region Constructors
        static Data()
        {
            Shapes = new Sprite[NumOfShapes, 4];

            #region Line
            {
                const ConsoleColor lineColor = ConsoleColor.Blue;

                var line0 = new Sprite();
                line0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', lineColor, lineColor));
                line0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lineColor, lineColor));
                line0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', lineColor, lineColor));
                line0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 3), ' ', lineColor, lineColor));
                Shapes[0, 0] = line0;

                var line1 = new Sprite();
                line1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', lineColor, lineColor));
                line1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lineColor, lineColor));
                line1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', lineColor, lineColor));
                line1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), ' ', lineColor, lineColor));
                Shapes[0, 1] = line1;

                var line2 = new Sprite();
                line2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', lineColor, lineColor));
                line2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', lineColor, lineColor));
                line2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', lineColor, lineColor));
                line2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 3), ' ', lineColor, lineColor));
                Shapes[0, 2] = line2;

                var line3 = new Sprite();
                line3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', lineColor, lineColor));
                line3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lineColor, lineColor));
                line3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', lineColor, lineColor));
                line3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), ' ', lineColor, lineColor));
                Shapes[0, 3] = line3;
            }
            #endregion

            #region Square
            {
                const ConsoleColor squareColor = ConsoleColor.Cyan;

                var square0 = new Sprite();
                square0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', squareColor, squareColor));
                square0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', squareColor, squareColor));
                square0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', squareColor, squareColor));
                square0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', squareColor, squareColor));
                Shapes[1, 0] = square0;

                var square1 = new Sprite();
                square1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', squareColor, squareColor));
                square1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', squareColor, squareColor));
                square1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', squareColor, squareColor));
                square1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', squareColor, squareColor));
                Shapes[1, 1] = square1;

                var square2 = new Sprite();
                square2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', squareColor, squareColor));
                square2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', squareColor, squareColor));
                square2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', squareColor, squareColor));
                square2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', squareColor, squareColor));
                Shapes[1, 2] = square2;

                var square3 = new Sprite();
                square3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', squareColor, squareColor));
                square3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', squareColor, squareColor));
                square3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', squareColor, squareColor));
                square3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', squareColor, squareColor));
                Shapes[1, 3] = square3;
            }
            #endregion

            #region L-Shape
            {
                const ConsoleColor lShapeColor = ConsoleColor.Red;

                var lShape0 = new Sprite();
                lShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', lShapeColor, lShapeColor));
                lShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', lShapeColor, lShapeColor));
                lShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lShapeColor, lShapeColor));
                lShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', lShapeColor, lShapeColor));
                Shapes[2, 0] = lShape0;

                var lShape1 = new Sprite();
                lShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', lShapeColor, lShapeColor));
                lShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lShapeColor, lShapeColor));
                lShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', lShapeColor, lShapeColor));
                lShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', lShapeColor, lShapeColor));
                Shapes[2, 1] = lShape1;

                var lShape2 = new Sprite();
                lShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', lShapeColor, lShapeColor));
                lShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lShapeColor, lShapeColor));
                lShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', lShapeColor, lShapeColor));
                lShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', lShapeColor, lShapeColor));
                Shapes[2, 2] = lShape2;

                var lShape3 = new Sprite();
                lShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', lShapeColor, lShapeColor));
                lShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', lShapeColor, lShapeColor));
                lShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', lShapeColor, lShapeColor));
                lShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), ' ', lShapeColor, lShapeColor));
                Shapes[2, 3] = lShape3;
            }
            #endregion

            #region J-Shape
            {
                const ConsoleColor jShapeColor = ConsoleColor.Green;

                var jShape0 = new Sprite();
                jShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', jShapeColor, jShapeColor));
                jShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', jShapeColor, jShapeColor));
                jShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', jShapeColor, jShapeColor));
                jShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', jShapeColor, jShapeColor));
                Shapes[3, 0] = jShape0;

                var jShape1 = new Sprite();
                jShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', jShapeColor, jShapeColor));
                jShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', jShapeColor, jShapeColor));
                jShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', jShapeColor, jShapeColor));
                jShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', jShapeColor, jShapeColor));
                Shapes[3, 1] = jShape1;

                var jShape2 = new Sprite();
                jShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', jShapeColor, jShapeColor));
                jShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', jShapeColor, jShapeColor));
                jShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', jShapeColor, jShapeColor));
                jShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), ' ', jShapeColor, jShapeColor));
                Shapes[3, 2] = jShape2;

                var jShape3 = new Sprite();
                jShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', jShapeColor, jShapeColor));
                jShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', jShapeColor, jShapeColor));
                jShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', jShapeColor, jShapeColor));
                jShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', jShapeColor, jShapeColor));
                Shapes[3, 3] = jShape3;
            }
            #endregion

            #region T-Shape
            {
                const ConsoleColor tShapeColor = ConsoleColor.Magenta;

                var tShape0 = new Sprite();
                tShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', tShapeColor, tShapeColor));
                tShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', tShapeColor, tShapeColor));
                tShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', tShapeColor, tShapeColor));
                tShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', tShapeColor, tShapeColor));
                Shapes[4, 0] = tShape0;

                var tShape1 = new Sprite();
                tShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', tShapeColor, tShapeColor));
                tShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', tShapeColor, tShapeColor));
                tShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', tShapeColor, tShapeColor));
                tShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', tShapeColor, tShapeColor));
                Shapes[4, 1] = tShape1;

                var tShape2 = new Sprite();
                tShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', tShapeColor, tShapeColor));
                tShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', tShapeColor, tShapeColor));
                tShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', tShapeColor, tShapeColor));
                tShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', tShapeColor, tShapeColor));
                Shapes[4, 2] = tShape2;

                var tShape3 = new Sprite();
                tShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', tShapeColor, tShapeColor));
                tShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', tShapeColor, tShapeColor));
                tShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', tShapeColor, tShapeColor));
                tShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', tShapeColor, tShapeColor));
                Shapes[4, 3] = tShape3;
            }
            #endregion

            #region Z-Shape
            {
                const ConsoleColor zShapeColor = ConsoleColor.White;

                var zShape0 = new Sprite();
                zShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', zShapeColor, zShapeColor));
                zShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', zShapeColor, zShapeColor));
                zShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', zShapeColor, zShapeColor));
                zShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), ' ', zShapeColor, zShapeColor));
                Shapes[5, 0] = zShape0;

                var zShape1 = new Sprite();
                zShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', zShapeColor, zShapeColor));
                zShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', zShapeColor, zShapeColor));
                zShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', zShapeColor, zShapeColor));
                zShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', zShapeColor, zShapeColor));
                Shapes[5, 1] = zShape1;

                var zShape2 = new Sprite();
                zShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', zShapeColor, zShapeColor));
                zShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', zShapeColor, zShapeColor));
                zShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', zShapeColor, zShapeColor));
                zShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', zShapeColor, zShapeColor));
                Shapes[5, 2] = zShape2;

                var zShape3 = new Sprite();
                zShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', zShapeColor, zShapeColor));
                zShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', zShapeColor, zShapeColor));
                zShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', zShapeColor, zShapeColor));
                zShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', zShapeColor, zShapeColor));
                Shapes[5, 3] = zShape3;
            }
            #endregion

            #region Z-Shape
            {
                const ConsoleColor sShapeColor = ConsoleColor.Yellow;

                var sShape0 = new Sprite();
                sShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), ' ', sShapeColor, sShapeColor));
                sShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', sShapeColor, sShapeColor));
                sShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', sShapeColor, sShapeColor));
                sShape0.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', sShapeColor, sShapeColor));
                Shapes[6, 0] = sShape0;

                var sShape1 = new Sprite();
                sShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', sShapeColor, sShapeColor));
                sShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 0), ' ', sShapeColor, sShapeColor));
                sShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), ' ', sShapeColor, sShapeColor));
                sShape1.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', sShapeColor, sShapeColor));
                Shapes[6, 1] = sShape1;

                var sShape2 = new Sprite();
                sShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', sShapeColor, sShapeColor));
                sShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', sShapeColor, sShapeColor));
                sShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', sShapeColor, sShapeColor));
                sShape2.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), ' ', sShapeColor, sShapeColor));
                Shapes[6, 2] = sShape2;

                var sShape3 = new Sprite();
                sShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), ' ', sShapeColor, sShapeColor));
                sShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), ' ', sShapeColor, sShapeColor));
                sShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), ' ', sShapeColor, sShapeColor));
                sShape3.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 2), ' ', sShapeColor, sShapeColor));
                Shapes[6, 3] = sShape3;
            }
            #endregion
        }
        #endregion

        #region Properties
        public static Sprite[,] Shapes { get; }
        public const int NumOfShapes = 7;
        #endregion
    }
}
