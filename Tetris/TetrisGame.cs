﻿using System;
using GameEngine;

namespace Tetris
{
    public class TetrisGame : Game
    {
        #region Constructors
        public TetrisGame()
        {
            Current = this;

            IterationSleep = 100;

            //Block sprite
            Block = new SpriteBlock();
            BlockLocation = new Point((Console.WindowWidth - Width) / 2, 0);

            //Bottom sprite
            Bottom = new Sprite();
            var bottomY = Console.WindowHeight - 1;
            for (var x = (Console.WindowWidth - Width) / 2; x <= (Console.WindowWidth + Width) / 2; x++)
            {
                Bottom.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(x, bottomY), 'X', ConsoleColor.Black));
            }
            Primitives.DrawHorizontalLine(bottomY, (Console.WindowWidth - Width) / 2 - 1, (Console.WindowWidth + Width) / 2 + 1, 'O', ConsoleColor.Gray, ConsoleColor.Gray);

            //LeftWall
            LeftWall = new Sprite();
            for(var y = 0; y < bottomY; y++)
            {
                LeftWall.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point((Console.WindowWidth - Width) / 2 - 1, y), 'X', ConsoleColor.Black));
            }
            Primitives.DrawVerticalLine((Console.WindowWidth - Width) / 2 - 1, 0, bottomY, ' ', ConsoleColor.Gray, ConsoleColor.Gray);

            //Right Wall
            RightWall = new Sprite();
            for (var y = 0; y < bottomY; y++)
            {
                RightWall.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point((Console.WindowWidth + Width) / 2 + 1, y), 'X', ConsoleColor.Black));
            }
            Primitives.DrawVerticalLine((Console.WindowWidth + Width) / 2 + 1, 0, bottomY, ' ', ConsoleColor.Gray, ConsoleColor.Gray);

            GameObjects.Add(new ScoreKeeper());
            GameObjects.Add(new FallingShape());
        }
        #endregion

        #region Properties
        public static TetrisGame Current { get; protected set; }

        public static int Score { get; set; }

        public SpriteBlock Block { get; set; }
        public Point BlockLocation { get; }

        public Sprite LeftWall { get; set; }
        public Sprite RightWall { get; set; }
        public Sprite Bottom { get; set; }
        #endregion

        #region Settings
        public const int Width = 20;
        #endregion
    }
}
