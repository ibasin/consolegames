﻿using System;
using GameEngine;

namespace Tetris
{
    public class FallingShape : TangibleGameObject
    {
        #region Constructors
        public FallingShape()
        {
            Location = new Point((Console.WindowWidth - 4)/2, 0);
            ShapeIdx = Game.Rnd.Next(Data.NumOfShapes);
            ShapeOrientationIdx = Game.Rnd.Next(4);
            if (IsCollidingWithSomething()) throw new GameOverException($"Game Over. Score: {TetrisGame.Score}");
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            for (var i = 0; i < 4; i++)
            {
                var key = Game.KeyboardManager.PeekKey();
                if (key != null)
                {
                    Game.KeyboardManager.ReadKey();
                    if (key.Value.Key == ConsoleKey.LeftArrow)
                    {
                        Location = Location.Move(DirectionEnum.Left);
                        if (IsCollidingWithSomething()) Location = Location.Move(DirectionEnum.Right);
                    }
                    if (key.Value.Key == ConsoleKey.RightArrow)
                    {
                        Location = Location.Move(DirectionEnum.Right);
                        if (IsCollidingWithSomething()) Location = Location.Move(DirectionEnum.Left);
                    }
                    if (key.Value.Key == ConsoleKey.UpArrow)
                    {
                        ShapeOrientationIdx--;
                        if (IsCollidingWithSomething()) ShapeOrientationIdx++;
                    }
                    if (key.Value.Key == ConsoleKey.DownArrow)
                    {
                        ShapeOrientationIdx++;
                        if (IsCollidingWithSomething()) ShapeOrientationIdx--;
                    }
                    if (key.Value.Key == ConsoleKey.Spacebar)
                    {
                        while (!ToDelete) MoveDown();
                        return;
                    }
                }
            }

            if (++_iteration % 4 == 0) MoveDown();
        }
        public override void Draw()
        {
            if (!ToDelete) Sprite.Draw(Location);
        }
        public override void Erase()
        {
            if (!ToDelete) Sprite.Erase(Location, ConsoleColor.Black);
        }
        #endregion

        #region Methods
        protected bool IsCollidingWithSomething()
        {
            var game = TetrisGame.Current;
            return game.LeftWall.IsCollidingAtLocation(Point.Zero, Sprite, Location) ||
                   game.RightWall.IsCollidingAtLocation(Point.Zero, Sprite, Location) ||
                   game.Block.IsCollidingAtLocation(game.BlockLocation, Sprite, Location) ||
                   game.Bottom.IsCollidingAtLocation(Point.Zero, Sprite, Location);
        }
        protected void MoveDown()
        {
            var newLocation = Location.Move(DirectionEnum.Down);
            var game = TetrisGame.Current;
            if (game.Block.IsCollidingAtLocation(game.BlockLocation, Sprite, newLocation) ||
                game.Bottom.IsCollidingAtLocation(Point.Zero, Sprite, newLocation))
            {
                game.Block.ConsumeOtherSprite(game.BlockLocation, Sprite, Location);
                Draw();
                ToDelete = true;
                game.GameObjects.Add(new FallingShape());

                var lines = game.Block.CountFullLines();
                if (lines > 0)
                {
                    Console.Beep();

                    game.Block.Erase(game.BlockLocation, ConsoleColor.Black);
                    game.Block.RemoveFullLines();
                    game.Block.Draw(game.BlockLocation);

                    //update score
                    if (lines == 1) TetrisGame.Score += 40;
                    else if (lines == 2) TetrisGame.Score += 100;
                    else if (lines == 3) TetrisGame.Score += 300;
                    else if (lines == 4) TetrisGame.Score += 1200;
                }
            }
            else
            {
                Location = newLocation;
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; }

        public int ShapeIdx
        {
            get => _shapeIdx;
            set
            {
                if (value < 0) value = Data.NumOfShapes - 1;
                if (value >= Data.NumOfShapes) value = 0;
                _shapeIdx = value;
            }
        }
        private int _shapeIdx;


        public int ShapeOrientationIdx
        {
            get => _shapeOrientationIdx;
            set
            {
                if (value < 0) value = 4 - 1;
                if (value >= 4) value = 0;
                _shapeOrientationIdx = value;
            }
        }
        private int _shapeOrientationIdx;

        public Sprite Sprite => Data.Shapes[ShapeIdx, ShapeOrientationIdx];
        protected int _iteration;
        #endregion
    }
}
