﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEngine;

namespace Tetris
{
    public class SpriteBlock : Sprite 
    {
        #region Methods
        public int CountFullLines()
        {
            int count = 0;
            for (var y = 0; y <= Console.WindowHeight - 1; y++)
            {
                if (PointsWithCharAndColor.Count(x => x.Point.Y == y) == TetrisGame.Width + 1) count++;
            }
            return count;
        }

        public void RemoveFullLines()
        {
            for (var y = 0; y <= Console.WindowHeight - 1; y++)
            {
                if (PointsWithCharAndColor.Count(x => x.Point.Y == y) == TetrisGame.Width + 1) RemoveLine(y);
            }
        }
        private void RemoveLine(int y)
        {
            var newPointsWithCharAndColor = new List<PointWithCharAndColor>();
            foreach (var pointWithCharAndColor in PointsWithCharAndColor)
            {
                //if equal, do not copy
                if (pointWithCharAndColor.Point.Y < y) newPointsWithCharAndColor.Add(pointWithCharAndColor.Move(DirectionEnum.Down));
                if (pointWithCharAndColor.Point.Y > y) newPointsWithCharAndColor.Add(pointWithCharAndColor);
            }
            PointsWithCharAndColor = newPointsWithCharAndColor;
        }
        #endregion
    }
}
