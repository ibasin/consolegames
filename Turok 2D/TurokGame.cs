﻿using System;
using GameEngine;

namespace Turok_2D
{
    public class TurokGame : Game
    {
        #region Constructors
        public TurokGame()
        {
            //Painting green grass at the bottom of the screen
            Primitives.DrawHorizontalLine(Console.WindowHeight - 1, 0, Console.WindowWidth - 1, '_',  ConsoleColor.Green);

            IterationSleep = 200;
            
            DinosaurHunter = new DinosaurHunter(new Point(Console.WindowWidth / 2 - 3 / 2, Console.WindowHeight - 4));
            GameObjects.Add(DinosaurHunter);

            Dinosaur = new Dinosaur();
            GameObjects.Add(Dinosaur);

            Current = this;
        }
        #endregion

        #region Properties
        public static TurokGame Current { get; private set; }
        
        public DinosaurHunter DinosaurHunter { get; set; }
        public Dinosaur Dinosaur { get; set; }
        #endregion
    }
}