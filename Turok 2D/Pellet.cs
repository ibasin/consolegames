﻿using System;
using GameEngine;

namespace Turok_2D
{
    public class Pellet : TangibleGameObject
    {
        #region Constructors
        public Pellet(Point location)
        {
            Body = new Sprite();
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 0), '-', ConsoleColor.Yellow));
            Body.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), ' ', ConsoleColor.Yellow));

            Location = location;
        }
        #endregion

        #region Overrides
        public override void Erase()
        {
            Body.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            Location = Location.Move(DirectionEnum.Right);

            if (!Body.IsFullyValidAtLocation(Location)) ToDelete = true;
            if (Body.IsCollidingAtLocation(Location, TurokGame.Current.Dinosaur.MySprite, TurokGame.Current.Dinosaur.Location))
            {
                //re-spawn the dinosaur
                TurokGame.Current.Dinosaur.Respawn();
                ToDelete = true;
            }
        }
        public override void Draw()
        {
            Body.Draw(Location);
        }
        #endregion

        #region Properties
        protected Point Location { get; set; }
        protected Sprite Body { get; }
        #endregion
    }
}

