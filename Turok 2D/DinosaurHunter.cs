﻿using GameEngine;
using System;

namespace Turok_2D
{
    public class DinosaurHunter : TangibleGameObject
    {
        #region Constructors
        public DinosaurHunter(Point location)
        {
            MySprite = new Sprite();

            //Head
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), 'O', ConsoleColor.DarkYellow));

            //Body
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 1), '|', ConsoleColor.DarkGreen));

            //Arms
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '-', ConsoleColor.DarkGreen));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '-', ConsoleColor.DarkGreen));

            //Legs
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 2), '/', ConsoleColor.DarkGreen));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 2), '\\', ConsoleColor.DarkGreen));

            //Shotgun
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(3, 1), '┳', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(4, 1), '═', ConsoleColor.DarkGray));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(5, 1), '-', ConsoleColor.DarkGray));

            Location = location;
        }
        #endregion

        #region Overrides
        public override void Erase()
        {
            MySprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = new Point(Location);
            CoolDownCounter++;
            
            while(true)
            {
                var key = TurokGame.KeyboardManager.ReadKey();
                if (!key.HasValue) break;
                
                if (key.Value.Key == ConsoleKey.Spacebar && CoolDownCounter > 2)
                {
                    var bullet = new Pellet(location + new Point(5, 1));
                    TurokGame.Current.GameObjects.Add(bullet);
                    CoolDownCounter = 0;
                }

                if (key.Value.Key == ConsoleKey.D) Location = Location.Move(DirectionEnum.Right);
                if (key.Value.Key == ConsoleKey.A)
                {
                    Location = Location.Move(DirectionEnum.Left);
                    while (TurokGame.KeyboardManager.ReadKey().HasValue) { /*Do nothing*/ }
                }
            }

            if (!MySprite.IsFullyValidAtLocation(Location)) Location = location;
        }
        public override void Draw()
        {
            MySprite.Draw(Location);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public  Sprite MySprite { get; }

        protected int CoolDownCounter { get; set; }
        #endregion
    }
}
