﻿using GameEngine;
using System;

namespace Turok_2D
{
    public class Dinosaur : TangibleGameObject
    {
        #region Constructors
        public Dinosaur()
        {
            MySprite = new Sprite();

            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(1, 0), '_', ConsoleColor.DarkYellow));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, 1), '|', ConsoleColor.DarkYellow));
            MySprite.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(2, 1), '\\', ConsoleColor.DarkYellow));

            Location = new Point(Console.WindowWidth - MySprite.Size.X - 1, Console.WindowHeight - MySprite.Size.Y - 1);
        }
        #endregion

        #region Overrides
        public override void Erase()
        {
            MySprite.Erase(Location, ConsoleColor.Black);
        }
        public override void Update()
        {
            var location = Location.Move(DirectionEnum.Left);
            if (!MySprite.IsFullyValidAtLocation(location)) 
            {
                Respawn();
                return;
            }

            if (MySprite.IsCollidingAtLocation(location, TurokGame.Current.DinosaurHunter.MySprite, TurokGame.Current.DinosaurHunter.Location))
            {
                throw new GameOverException("You have been eaten! Yum!");
            }

            Location = location;
        }
        public override void Draw()
        {
            MySprite.Draw(Location);
        }
        #endregion

        #region Methods
        public void Respawn()
        {
            ToDelete = true;

            TurokGame.Current.Dinosaur = new Dinosaur();
            TurokGame.Current.GameObjects.Add(TurokGame.Current.Dinosaur);
        }
        #endregion

        #region Properties
        public Point Location { get; protected set; }
        public Sprite MySprite { get; }
        #endregion
    }
}

