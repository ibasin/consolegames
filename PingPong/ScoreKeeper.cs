﻿using System;
using GameEngine;

namespace PingPong
{
    public class ScoreKeeper : GameObject
    {
        #region Overrides
        public override void Update()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.BackgroundColor = ConsoleColor.Black;
            string scoreStr = $"{PingPongGame.LeftScore}:{PingPongGame.RightScore}";
            Console.SetCursorPosition((Console.WindowWidth - scoreStr.Length)/2, 0);
            Console.Write(scoreStr);
        }
        #endregion
    }
}
