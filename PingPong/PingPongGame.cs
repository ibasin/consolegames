﻿using GameEngine;

namespace PingPong
{
    public class PingPongGame : Game
    {
        #region Constructors
        public PingPongGame()
        {
            Current = this;
            GameObjects.Add(new ScoreKeeper());
            GameObjects.Add(new LeftPaddle(5));
            GameObjects.Add(new RightPaddle(5));
            GameObjects.Add(new Ball());
            IterationSleep = 75;
        }
        #endregion

        #region Properties
        public static PingPongGame Current { get; protected set; }
        public static int LeftScore { get; set; }
        public static int RightScore { get; set; }
        #endregion
    }
}
