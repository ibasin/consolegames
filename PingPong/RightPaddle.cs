﻿using System;
using GameEngine;

namespace PingPong
{
    public class RightPaddle : Paddle
    {
        #region Constructors
        public RightPaddle(int size) : base(size)
        {
            Location = new Point(Console.WindowWidth - 4, (Console.WindowHeight - size) / 2);
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.PeekKey();
            if (key != null)
            {
                if (key.Value.Key == ConsoleKey.P)
                {
                    Game.KeyboardManager.ReadKey();
                    MoveUp();
                }
                if (key.Value.Key == ConsoleKey.L)
                {
                    Game.KeyboardManager.ReadKey();
                    MoveDown();
                }
            }
        }
        #endregion
    }
}
