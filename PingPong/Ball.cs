﻿using System;
using GameEngine;

namespace PingPong
{
    public class Ball : TangibleGameObject
    {
        #region Constructors
        public Ball(int? vx = null)
        {

            X = Math.Round((double)Console.WindowWidth / 2);
            Y = Math.Round((double)Console.WindowHeight / 2);
            Location = new Point((int)X, (int)Y);

            if (vx == null) vx = Game.Rnd.Next(1) == 0 ? -1 : 1;
            VX = vx.Value;
            VY = Game.Rnd.NextDouble()*2.4 - 1.2;
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            Console.BackgroundColor = ConsoleColor.Yellow;
            Location.SetCursorPosition();
            Console.Write(" ");
        }
        public override void Erase()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Location.SetCursorPosition();
            Console.Write(" ");
        }
        public override void Update()
        {
            X += VX;
            Y += VY;
            Location = new Point((int)Math.Round(X), (int)Math.Round(Y));
            if (Location.Violations.XPlusViolation)
            {
                Location = Location.CorrectViolations();
                PingPongGame.LeftScore++;
                ReSpawnBall(-1);
            }
            if (Location.Violations.XMinusViolation)
            {
                Location = Location.CorrectViolations();
                PingPongGame.RightScore++;
                ReSpawnBall(1);
            }
            if (Location.Violations.YMinusViolation || Location.Violations.YPlusViolation)
            {
                Location = Location.CorrectViolations();
                VY = -VY;
            }
        }
        #endregion

        #region Methods
        public void ReSpawnBall(int vx)
        {
            ToDelete = true;
            PingPongGame.Current.GameObjects.Add(new Ball(vx));
        }
        #endregion

        #region Properties
        public Point Location { get; set; }

        public double X { get; set; }
        public double Y { get; set; }

        // ReSharper disable InconsistentNaming
        public double VX { get; set; }
        public double VY { get; set; }
        // ReSharper restore InconsistentNaming
        #endregion
    }
}
