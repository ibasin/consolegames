﻿using System;
using System.Linq;
using GameEngine;

namespace PingPong
{
    public abstract class Paddle : TangibleGameObject
    {
        #region Constructors
        protected Paddle(int size)
        {
            for (int y = 0; y < size; y++)
            {
                Surface.PointsWithCharAndColor.Add(new PointWithCharAndColor(new Point(0, y), ' ', ConsoleColor.White, ConsoleColor.White));
            }
        }
        #endregion

        #region Overrides
        public override void Draw()
        {
            Surface.Draw(Location);
        }
        public override void Erase()
        {
            Surface.Erase(Location, ConsoleColor.Black);
        }
        public override void PreUpdate()
        {
            Speed = 0;
            base.PostUpdate();
        }
        public override void PostUpdate()
        {
            var ball = (Ball)PingPongGame.Current.GameObjects.Single(x => !x.ToDelete && x is Ball);
            if (Surface.IsCollidingAtLocation(Location, ball.Location))
            {
                ball.VX = -ball.VX;
                ball.VY += (double)Speed / 2 + (Game.Rnd.NextDouble()*0.25 - 0.125); //we slightly randomize the hit
            }
            base.PostUpdate();
        }

        #endregion

        #region Methods
        public void MoveUp()
        {
            var newLocation = Location.Move(DirectionEnum.Up);
            if (Surface.IsFullyValidAtLocation(newLocation))
            {
                Location = newLocation;
                Speed = -1;
            }
        }
        public void MoveDown()
        {
            var newLocation = Location.Move(DirectionEnum.Down);
            if (Surface.IsFullyValidAtLocation(newLocation))
            {
                Location = newLocation;
                Speed = 1;
            }
        }
        #endregion

        #region Properties
        public Point Location { get; set; } = Point.Zero;
        public Sprite Surface { get; set; } = new Sprite();
        public int Speed { get; set; }
        #endregion
    }
}
