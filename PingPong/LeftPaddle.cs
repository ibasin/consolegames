﻿using System;
using GameEngine;

namespace PingPong
{
    public class LeftPaddle : Paddle
    {
        #region Constructors
        public LeftPaddle(int size) : base(size)
        {
            Location = new Point(3, (Console.WindowHeight - size)/ 2);
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            var key = Game.KeyboardManager.PeekKey();
            if (key != null)
            {
                if (key.Value.Key == ConsoleKey.Q)
                {
                    Game.KeyboardManager.ReadKey();
                    MoveUp();
                }
                if (key.Value.Key == ConsoleKey.A)
                {
                    Game.KeyboardManager.ReadKey();
                    MoveDown();
                }
            }
        }
        #endregion
    }
}
