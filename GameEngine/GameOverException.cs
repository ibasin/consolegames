﻿using System;

namespace GameEngine
{
    public class GameOverException : Exception
    {
        public GameOverException(string msg) : base(msg) { }
    }
}
