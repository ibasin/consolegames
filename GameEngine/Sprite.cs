﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameEngine
{
    public class Sprite
    {
        #region EmbeddedType
        public enum ConsumptionConflictResolutionEnum
        {
            ThrowException, BaseSpriteWins, OtherSpriteWins
        }
        #endregion

        #region Methods
        public void ConsumeOtherSprite(Point myLocation, Sprite other, Point otherLocation, ConsumptionConflictResolutionEnum resolution = ConsumptionConflictResolutionEnum.ThrowException)
        {
            var locationDelta = myLocation - otherLocation;
            foreach (var otherPointWithCharAndColor in other.PointsWithCharAndColor)
            {
                var newPoint = otherPointWithCharAndColor.Point - locationDelta;
                var myMatchingPoint = PointsWithCharAndColor.SingleOrDefault(x => x.Point == newPoint);
                if (myMatchingPoint != null)
                {
                    if (resolution == ConsumptionConflictResolutionEnum.ThrowException)
                    {
                        throw new ArgumentException("Sprites have point conflicts");
                    }
                    else if (resolution == ConsumptionConflictResolutionEnum.OtherSpriteWins)
                    {
                        PointsWithCharAndColor.Remove(myMatchingPoint);
                        var newPointWithCharAndColor = new PointWithCharAndColor(newPoint, otherPointWithCharAndColor.Chr,
                            otherPointWithCharAndColor.FgColor, otherPointWithCharAndColor.BgColor);
                        PointsWithCharAndColor.Add(newPointWithCharAndColor);
                    }
                    else if(resolution == ConsumptionConflictResolutionEnum.BaseSpriteWins)
                    {
                        PointsWithCharAndColor.Remove(myMatchingPoint);
                        var newPointWithCharAndColor = new PointWithCharAndColor(newPoint, myMatchingPoint.Chr,
                            myMatchingPoint.FgColor, myMatchingPoint.BgColor);
                        PointsWithCharAndColor.Add(newPointWithCharAndColor);
                    }
                    else
                    {
                        throw new ArgumentException("Invalid Conflict Resolution");
                    }
                }
                else
                {
                    var newPointWithCharAndColor = new PointWithCharAndColor(newPoint, otherPointWithCharAndColor.Chr,
                        otherPointWithCharAndColor.FgColor, otherPointWithCharAndColor.BgColor);
                    PointsWithCharAndColor.Add(newPointWithCharAndColor);
                }
            }
        }
        public bool Draw(Point location)
        {
            var result = true;
            foreach (var pointWithCharAndColor in PointsWithCharAndColor)
            {
                var pointResult = pointWithCharAndColor.Draw(location);
                result &= pointResult;
            }
            return result;
        }
        public bool Erase(Point location, ConsoleColor bgColor)
        {
            var result = true;
            foreach (var pointWithCharAndColor in PointsWithCharAndColor)
            {
                var pointResult = pointWithCharAndColor.Erase(location, bgColor);
                result &= pointResult;
            }
            return result;
        }
        public bool IsFullyValidAtLocation(Point location)
        {
            foreach (var pointWithCharAndColor in PointsWithCharAndColor)
            {
                if (!pointWithCharAndColor.IsValidAtLocation(location)) return false;
            }
            return true;
        }
        public bool IsCollidingAtLocation(Point myLocation, Point point)
        {
            foreach (var myPointWithCharAndColor in PointsWithCharAndColor)
            {
                if (myPointWithCharAndColor.Point + myLocation == point) return true;
            }
            return false;
        }
        public bool IsCollidingAtLocation(Point myLocation, Sprite otherSprite, Point otherSpriteLocation)
        {
            foreach (var myPointWithCharAndColor in PointsWithCharAndColor)
            {
                foreach (var otherPointWithCharAndColor in otherSprite.PointsWithCharAndColor)
                {
                    if (myPointWithCharAndColor.Point + myLocation == otherPointWithCharAndColor.Point + otherSpriteLocation) return true;
                }
            }
            return false;
        }
        public Point Size
        {
            get
            {
                var first = PointsWithCharAndColor.FirstOrDefault();
                if (first == null) return Point.Zero;

                var maxX = first.Point.X;
                var maxY = first.Point.Y;
                var minX = first.Point.X;
                var minY = first.Point.Y;

                foreach (var pointWithCharAndColor in PointsWithCharAndColor)
                {
                    if (pointWithCharAndColor.Point.X > maxX) maxX = pointWithCharAndColor.Point.X;
                    if (pointWithCharAndColor.Point.Y > maxY) maxY = pointWithCharAndColor.Point.Y;
                    if (pointWithCharAndColor.Point.X < minX) minX = pointWithCharAndColor.Point.X;
                    if (pointWithCharAndColor.Point.Y < minY) minY = pointWithCharAndColor.Point.Y;
                }

                return new Point(Math.Abs(maxX - minX) + 1, Math.Abs(maxY - minY) + 1);
            }
        }
        #endregion

        #region Properties
        public List<PointWithCharAndColor> PointsWithCharAndColor { get; protected set; } = new List<PointWithCharAndColor>();
        #endregion
    }
}
