﻿using System;

namespace GameEngine
{
    public struct Point
    {
        #region Embedded Types
        public struct PointViolations
        {
            #region Constructors
            public PointViolations(bool xMinusViolation, bool xPlusViolation, bool yMinusViolation, bool yPlusViolation)
            {
                XMinusViolation = xMinusViolation;
                XPlusViolation = xPlusViolation;
                YMinusViolation = yMinusViolation;
                YPlusViolation = yPlusViolation;
            }
            public PointViolations(PointViolations other)
            {
                XMinusViolation = other.XMinusViolation;
                XPlusViolation = other.XPlusViolation;
                YMinusViolation = other.YMinusViolation;
                YPlusViolation = other.YPlusViolation;
            }
            public PointViolations(Point point) : this(false, false, false, false)
            {
                if (point.X > Console.WindowWidth - 1) XPlusViolation = true;
                else if (point.X < 0) XMinusViolation = true;

                if (point.Y > Console.WindowHeight - 1) YPlusViolation = true;
                else if (point.Y < 0) YMinusViolation = true;
            }
            #endregion

            #region Properties
            public bool IsInViolation => XMinusViolation || XPlusViolation || YMinusViolation || YPlusViolation;
            public bool XMinusViolation { get; }
            public bool XPlusViolation { get; }
            public bool YMinusViolation { get; }
            public bool YPlusViolation { get; }
            #endregion
        }
        #endregion

        #region Constructors
        public Point(int x, int y)
        {
            X = x;
            Y = y;
            _violations = null;
        }
        public Point(Point other) : this(other.X, other.Y) { }

        public static Point CreateRandom()
        {
            return new Point(Game.Rnd.Next(Console.WindowWidth), Game.Rnd.Next(Console.WindowHeight));
        }
        #endregion

        #region Overrdies
        public override string ToString()
        {
            return $"({X},{Y})";
        }
        #endregion

        #region Operator Overloading
        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }
        public static bool operator ==(Point obj1, Point obj2)
        {
            return obj1.X == obj2.X && obj1.Y == obj2.Y;
        }
        public static bool operator !=(Point obj1, Point obj2)
        {
            return !(obj1.X == obj2.X && obj1.Y == obj2.Y);
        }
        public bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Point other && Equals(other);
        }
        public override int GetHashCode()
        {
            unchecked { return (X * 397) ^ Y; }
        }
        #endregion

        #region Methods
        public Point Move(DirectionEnum direction, int distance = 1)
        {
            Point point;
            if (direction == DirectionEnum.Up) point = new Point(X, Y - distance);
            else if (direction == DirectionEnum.Down) point = new Point(X, Y + distance);
            else if (direction == DirectionEnum.Right) point = new Point(X + distance, Y);
            else if (direction == DirectionEnum.Left) point = new Point(X - distance, Y);
            else throw new Exception("Invalid direction");
            return point;
        }
        public Point CorrectViolations()
        {
            int x, y;

            if (X > Console.WindowWidth - 1) x = Console.WindowWidth - 1;
            else if (X < 0) x = 0;
            else x = X;

            if (Y > Console.WindowHeight - 1) y = Console.WindowHeight - 1;
            else if (Y < 0) y = 0;
            else y = Y;

            return new Point(x, y);
        }
        public void SetCursorPosition()
        {
            Console.SetCursorPosition(X, Y);
        }
        #endregion

        #region Properties
        public static Point Zero { get; } = new Point(0, 0);

        public int X { get; }
        public int Y { get; }

        public PointViolations Violations
        {
            get
            {
                if (_violations == null) _violations = new PointViolations(this);
                return _violations.Value;
            }
        }
        private PointViolations? _violations;
        #endregion
    }
}
