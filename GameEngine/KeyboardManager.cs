﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameEngine
{
    public class KeyboardManager : GameObject
    {
        #region Methods
        public ConsoleKeyInfo? PeekKey()
        {
            if (!KeyBuffer.Any()) return null;
            return KeyBuffer.First();
        }
        public ConsoleKeyInfo? ReadKey()
        {
            var key = PeekKey();
            if (key != null) KeyBuffer.RemoveAt(0);
            return key;
        }
        #endregion

        #region Overrides
        public override void Update()
        {
            //we can read up to 10 keys per iteration
            for(var i = 0; i < 10; i++)
            {
                if (Console.KeyAvailable) 
                {
                    var keyInfo = Console.ReadKey(true);
                    
                    if (keyInfo.Key == PauseButton) WaitForUnpause();
                    else KeyBuffer.Insert(0, keyInfo);
                }
            }
        }
        protected virtual void WaitForUnpause()
        {
            while(true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == PauseButton) break;
                Thread.Sleep(50);
            }
        }
        #endregion

        #region Properties
        protected List<ConsoleKeyInfo> KeyBuffer { get; } = new List<ConsoleKeyInfo>();
        public ConsoleKey PauseButton { get; set; } = ConsoleKey.Backspace;
        #endregion
    }
}
