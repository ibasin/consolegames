﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameEngine
{
    public abstract class Game
    {
        #region Methods
        public virtual void Run()
        {
            //Set console to Unicode
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            
            //Hide Cursor make bg black
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;

            //Initial show all objects
            foreach (var gameObject in GameObjects)
            {
                if (gameObject is TangibleGameObject tangibleGameObject) tangibleGameObject.Draw();
            }

            try
            {
                while (true)
                {
                    //Run keyboard manager
                    KeyboardManager.Update();
                    
                    //Round robin pre-update all game objects
                    foreach (var gameObject in GameObjects.ToArray()) gameObject.PreUpdate();

                    //Round robin update all game objects
                    foreach (var gameObject in GameObjects.ToArray())
                    {
                        var tangibleGameObject = gameObject as TangibleGameObject;

                        tangibleGameObject?.Erase();
                        gameObject.Update();
                        tangibleGameObject?.Draw();
                    }

                    //Round robin post-update all game objects
                    foreach (var gameObject in GameObjects.ToArray()) gameObject.PostUpdate();

                    //Delete kills
                    foreach (var gameObject in GameObjects.Where(x => x.ToDelete).ToArray())
                    {
                        if (gameObject is TangibleGameObject tangibleGameObject) tangibleGameObject.Erase();
                        GameObjects.Remove(gameObject);
                    }

                    Thread.Sleep(IterationSleep);
                }
            }
            catch (GameOverException ex)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Clear();
                Console.SetCursorPosition((Console.WindowWidth - ex.Message.Length)/2, Console.WindowHeight / 2);
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
        #endregion

        #region Properties
        public static Random Rnd { get; set; } = new Random(Guid.NewGuid().GetHashCode());
        public static KeyboardManager KeyboardManager { get; } = new KeyboardManager();

        public List<GameObject> GameObjects { get; set; } = new List<GameObject>();
        protected int IterationSleep { get; set; } = 50;
        #endregion
    }
}
