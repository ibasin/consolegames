﻿namespace GameEngine
{
    public abstract class GameObject
    {
        #region Methods
        public virtual void PreUpdate() { }
        public abstract void Update();
        public virtual void PostUpdate() { }
        #endregion

        #region Properties
        public virtual bool ToDelete { get; set; }
        #endregion
    }
}