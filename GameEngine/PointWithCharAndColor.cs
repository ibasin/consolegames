﻿using System;

namespace GameEngine
{
    public class PointWithCharAndColor
    {
        #region Constructors
        public PointWithCharAndColor(Point point, char chr, ConsoleColor fgColor, ConsoleColor bgColor = ConsoleColor.Black)
        {
            Point = point;
            Chr = chr;
            FgColor = fgColor;
            BgColor = bgColor;
        }
        public PointWithCharAndColor(PointWithCharAndColor other)
            :this(other.Point, other.Chr, other.FgColor, other.BgColor) {}
        #endregion

        #region Methods
        public PointWithCharAndColor Move(DirectionEnum direction, int distance = 1)
        {
            return new PointWithCharAndColor(Point.Move(direction, distance), Chr, FgColor, BgColor);
        }
        public bool IsValidAtLocation(Point location)
        {
            var pointAtLocation = Point + location;
            return !pointAtLocation.Violations.IsInViolation;
        }
        public bool Draw(Point location)
        {
            Console.ForegroundColor = FgColor;
            Console.BackgroundColor = BgColor;
            return Draw(location, Chr);
        }
        public bool Erase(Point location, ConsoleColor bgColor)
        {
            Console.BackgroundColor = bgColor;
            return Draw(location, ' ');
        }
        protected bool Draw(Point location, char chr)
        {
            var pointAtLocation = Point + location;
            if (pointAtLocation.Violations.IsInViolation) return false;
            pointAtLocation.SetCursorPosition();
            Console.Write(chr);
            return true;
        }
        #endregion

        #region Properties
        public Point Point { get; }
        public char Chr { get; }
        public ConsoleColor FgColor { get; }
        public ConsoleColor BgColor { get; }
        #endregion
    }
}
