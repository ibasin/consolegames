﻿using System;

namespace GameEngine
{
    public static class Primitives
    {
        public static void DrawVerticalLine(int x, int y1, int y2, char chr, ConsoleColor fgColor, ConsoleColor bgColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = fgColor;
            Console.BackgroundColor = bgColor;
            if (y1 > y2)
            {
                var tmp = y1;
                y1 = y2;
                y2 = tmp;
            }
            
            for (var y = y1; y <= y2; y++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(chr);
            }
        }
        public static void DrawHorizontalLine(int y, int x1, int x2, char chr, ConsoleColor fgColor, ConsoleColor bgColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = fgColor;
            Console.BackgroundColor = bgColor;
            if (x1 > x2)
            {
                var tmp = x1;
                x1 = x2;
                x2 = tmp;
            }
            
            for (var x = x1; x <= x2; x++)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(chr);
            }
        }
    }
}
