﻿namespace GameEngine
{
    public abstract class TangibleGameObject : GameObject
    {
        #region Methods
        public abstract void Draw();
        public abstract void Erase();
        #endregion
    }
}